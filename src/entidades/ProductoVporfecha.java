/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author TASHZ
 */
public class ProductoVporfecha {
    private String Producto;
    private Double Precio_Unitario;
    private Double Ganancia;
    private int Cantidad_vendida;

    public ProductoVporfecha(String Producto, Double Preciouni, int cantv,Double Ganancia) {
        this.Producto = Producto;
        this.Precio_Unitario = Preciouni;
        this.Ganancia = Ganancia;
        this.Cantidad_vendida = cantv;
    }

    public String getProducto() {
        return Producto;
    }

    public void setProducto(String ProductosV1) {
        this.Producto = ProductosV1;
    }

    public Double getPrecio_Unitario() {
        return Precio_Unitario;
    }

    public void setPrecio_Unitario(Double Preciouni) {
        this.Precio_Unitario = Preciouni;
    }

    public Double getGanancia() {
        return Ganancia;
    }

    public void setGanancia(Double Ganancia) {
        this.Ganancia = Ganancia;
    }

    public int getCantidad_vendida() {
        return Cantidad_vendida;
    }

    public void setCantidad_vendida(int cantv) {
        this.Cantidad_vendida = cantv;
    }
    
}
