
package entidades;

/**
 *
 * @author Vargas
 */
public class Proveedor {
    private int Proveedor_ID;
    private String nombre,Web;

    public Proveedor(int Proveedor_ID, String nombre, String Web) {
        this.Proveedor_ID = Proveedor_ID;
        this.nombre = nombre;
        this.Web = Web;
    }

    public int getProveedor_ID() {
        return Proveedor_ID;
    }

    public void setProveedor_ID(int Proveedor_ID) {
        this.Proveedor_ID = Proveedor_ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getWeb() {
        return Web;
    }

    public void setWeb(String Web) {
        this.Web = Web;
    }

    
}
