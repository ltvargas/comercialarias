/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author TASHZ
 */
public class ProductoNoVporfecha {
    private String Producto;
    private String Marca;
    private int Stock;
    private Double Precio_Unitario;

    public ProductoNoVporfecha(String Producto, String Marca, int Stock, Double Precio_Unitario) {
        this.Producto = Producto;
        this.Marca = Marca;
        this.Stock = Stock;
        this.Precio_Unitario = Precio_Unitario;
    }

    public String getProducto() {
        return Producto;
    }

    public void setProducto(String Producto) {
        this.Producto = Producto;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int Stock) {
        this.Stock = Stock;
    }

    public Double getPrecio_Unitario() {
        return Precio_Unitario;
    }

    public void setPrecio_Unitario(Double Precio_Unitario) {
        this.Precio_Unitario = Precio_Unitario;
    }
    

    
}
