
package entidades;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Vargas
 */
public class Pedido {
    private int Pedido_ID;
    private Date Fecha_Pedida;
    private Date Fecha_Entrega;
    private String Proveedor_ID;
    private int Empleado_ID;

    public Pedido(int Pedido_ID, Date Fecha_Pedida, Date Fecha_Entrega, String Proveedor_ID, int Empleado_ID) {
        this.Pedido_ID = Pedido_ID;
        this.Fecha_Pedida = Fecha_Pedida;
        this.Fecha_Entrega = Fecha_Entrega;
        this.Proveedor_ID = Proveedor_ID;
        this.Empleado_ID = Empleado_ID;
    }

    public int getPedido_ID() {
        return Pedido_ID;
    }

    public void setPedido_ID(int Pedido_ID) {
        this.Pedido_ID = Pedido_ID;
    }

    public Date getFecha_Pedida() {
        return Fecha_Pedida;
    }

    public void setFecha_Pedida(Date Fecha_Pedida) {
        this.Fecha_Pedida = Fecha_Pedida;
    }

    public Date getFecha_Entrega() {
        return Fecha_Entrega;
    }

    public void setFecha_Entrega(Date Fecha_Entrega) {
        this.Fecha_Entrega = Fecha_Entrega;
    }

    public String getProveedor_ID() {
        return Proveedor_ID;
    }

    public void setProveedor_ID(String Proveedor_ID) {
        this.Proveedor_ID = Proveedor_ID;
    }

    public int getEmpleado_ID() {
        return Empleado_ID;
    }

    public void setEmpleado_ID(int Empleado_ID) {
        this.Empleado_ID = Empleado_ID;
    }

    
}

    