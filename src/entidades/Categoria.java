
package entidades;

/**
 *
 * @author Vargas
 */
public class Categoria {
    private int Categoria_ID;
    private String Categoria,descripcion;

    public Categoria(int Categoria_ID, String Categoria, String descripcion) {
        this.Categoria_ID = Categoria_ID;
        this.Categoria = Categoria;
        this.descripcion = descripcion;
    }

    public int getCategoria_ID() {
        return Categoria_ID;
    }

    public void setCategoria_ID(int Categoria_ID) {
        this.Categoria_ID = Categoria_ID;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    
    
    
    
    }