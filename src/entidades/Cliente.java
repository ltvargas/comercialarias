
package entidades;

/**
 *
 * @author Vargas
 */
public class Cliente {
    private int cliente_ID;
    private String nombre,apellido,direccion,telefono;

    public Cliente(int cliente_ID, String nombre, String apellido,String direccion,  String telefono) {
        this.cliente_ID = cliente_ID;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion=direccion;
        this.telefono = telefono;
    }

    public int getCliente_ID() {
        return cliente_ID;
    }

    public void setCliente_ID(int cliente_ID) {
        this.cliente_ID = cliente_ID;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
