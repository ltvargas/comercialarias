/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author TASHZ
 */
public class MasCompras {
    private int NumeroCompras;
    private String Nombre;
    private String Apellido;

    public MasCompras(int NumeroCompras, String Nombre, String Apellido) {
        this.NumeroCompras = NumeroCompras;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
    }

    public int getNumeroCompras() {
        return NumeroCompras;
    }

    public void setNumeroCompras(int NumeroCompras) {
        this.NumeroCompras = NumeroCompras;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }
    
}
