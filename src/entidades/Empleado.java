
package entidades;

import java.sql.Date;

/**
 *
 * @author Vargas
 */
public class Empleado {
    private int Empleado_ID;
    private String nombre,apellido,telefono;
    private Date Fecha_Nacimiento;

    public Empleado(int Empleado_ID, String nombre, String apellido, Date Fecha_nacimiento, String telefono) {
        this.Empleado_ID = Empleado_ID;
        this.nombre = nombre;
        this.apellido = apellido;
        this.Fecha_Nacimiento=Fecha_nacimiento;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getEmpleado_ID() {
        return Empleado_ID;
    }

    public void setEmpleado_ID(int Empleado_ID) {
        this.Empleado_ID = Empleado_ID;
    }

    public Date getFecha_Nacimiento() {
        return Fecha_Nacimiento;
    }

    public void setFecha_Nacimiento(Date Fecha_Nacimiento) {
        this.Fecha_Nacimiento = Fecha_Nacimiento;
    }

    
  
    
    
}
