
package entidades;

/**
 *
 * @author Vargas
 */
public class Producto {
    private String nombre,marca,Proveedor_ID;
    private int Producto_ID,stock,Categoria_ID;
    private double precio_Venta;

    public Producto(int Producto_ID,String nombre, String marca,  int stock, double precio_Venta, int Categoria_ID, String Proveedor_ID) {
        this.nombre = nombre;
        this.marca = marca;
        this.Proveedor_ID= Proveedor_ID;
        this.Producto_ID= Producto_ID;
        this.stock = stock;
        this.precio_Venta=precio_Venta;
        this.Categoria_ID=Categoria_ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getProveedor_ID() {
        return Proveedor_ID;
    }

    public void setProveedor_ID(String Proveedor_ID) {
        this.Proveedor_ID = Proveedor_ID;
    }

    public int getCategoria_ID() {
        return Categoria_ID;
    }

    public void setCategoria_ID(int Categoria_ID) {
        this.Categoria_ID = Categoria_ID;
    }

    
    public int getProducto_ID() {
        return Producto_ID;
    }

    public void setProducto_ID(int Producto_ID) {
        this.Producto_ID = Producto_ID;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getPrecio_Venta() {
        return precio_Venta;
    }

    public void setPrecio_Venta(double precio_Venta) {
        this.precio_Venta = precio_Venta;
    }

    
    
}