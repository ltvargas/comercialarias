/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author TASHZ
 */
public class ProductoporCategoria {
    private int ProductosVendidos;
    private String Categoria;

    public ProductoporCategoria(int productosvendidos, String Categoria) {
        this.ProductosVendidos = productosvendidos;
        this.Categoria = Categoria;
    }

    public int getProductosVendidos() {
        return ProductosVendidos;
    }

    public void setProductosVendidos(int ProductosVendidos) {
        this.ProductosVendidos = ProductosVendidos;
    }

    
    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }
    
    
}
