
package entidades;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Vargas
 */
public class Venta {
    private int Venta_ID,Cliente_ID;
    private Date fecha;
    private double descuento,Monto_Final;
    private ArrayList<DetalleVenta> detalleV;

    public Venta(int Venta_ID, Date fecha, double descuento, double Monto_Final,int Cliente_ID) {
        this.Venta_ID = Venta_ID;
        this.fecha = fecha;
        this.descuento = descuento;
        this.Monto_Final = Monto_Final;
        this.Cliente_ID=Cliente_ID;
        detalleV=new ArrayList<>();
    }

    public int getVenta_ID() {
        return Venta_ID;
    }

    public void setVenta_ID(int Venta_ID) {
        this.Venta_ID = Venta_ID;
    }

    public int getCliente_ID() {
        return Cliente_ID;
    }

    public void setCliente_ID(int Cliente_ID) {
        this.Cliente_ID = Cliente_ID;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getMonto_Final() {
        return Monto_Final;
    }

    public void setMonto_Final(double Monto_Final) {
        this.Monto_Final = Monto_Final;
    }

    public ArrayList<DetalleVenta> getDetalleV() {
        return detalleV;
    }

    public void setDetalleV(ArrayList<DetalleVenta> detalleV) {
        this.detalleV = detalleV;
    }

    
    
    
    
    
    
}