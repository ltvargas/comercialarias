
package entidades;

/**
 *
 * @author Vargas
 */
public class DetalleVenta {
    private int Unidades,Producto_ID;
    private String Nombre_Producto;
    private double Subtotal,Precio_Unitario;

    public DetalleVenta(int Producto_ID,String Nombre_Producto, int Unidades,double Precio_Unitario, double Subtotal) {
        this.Unidades = Unidades;
        this.Subtotal = Subtotal;
        this.Precio_Unitario=Precio_Unitario;
        this.Nombre_Producto=Nombre_Producto;
        this.Producto_ID= Producto_ID;
    }

    public int getProducto_ID() {
        return Producto_ID;
    }

    public void setProducto_ID(int Producto_ID) {
        this.Producto_ID = Producto_ID;
    }

    public double getPrecio_Unitario() {
        return Precio_Unitario;
    }

    public void setPrecio_Unitario(double Precio_Unitario) {
        this.Precio_Unitario = Precio_Unitario;
    }

    public String getNombre_Producto() {
        return Nombre_Producto;
    }

    public void setNombre_Producto(String Nombre_Producto) {
        this.Nombre_Producto = Nombre_Producto;
    }


    public int getUnidades() {
        return Unidades;
    }

    public void setUnidades(int Unidades) {
        this.Unidades = Unidades;
    }

    public double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(double Subtotal) {
        this.Subtotal = Subtotal;
    }

}