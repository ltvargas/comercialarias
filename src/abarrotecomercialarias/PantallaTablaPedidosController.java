/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.ventPrincipal;
import static abarrotecomercialarias.ComercialAriasAcceso.ventincio;
import static abarrotecomercialarias.FXMLAccesoController.getVa;
import static abarrotecomercialarias.Mensaje.ventanaEmergente;
import entidades.Pedido;
import entidades.Producto;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaTablaPedidosController implements Initializable {
@FXML
    private Label label;
    @FXML
    private ImageView bproducto;
    @FXML
    private Button producto;
    @FXML
    private Button venta;
    @FXML
    private Button pedido;
    @FXML
    private Button proveedor;
    @FXML
    private Button categoria;
    @FXML
    private Button empleado;
    @FXML
    private ImageView bventa;
    @FXML
    private ImageView vpedido;
    @FXML
    private ImageView bproveedor;
    @FXML
    private ImageView bcategoria;
    @FXML
    private ImageView bempleado;
    @FXML
    private ComboBox<?> comboxbus;
    @FXML
    private ImageView borrartodo;
    @FXML
    private Button categoria1;
    @FXML
    private ImageView bempleado1;
    @FXML
    private TableView<Pedido> tablaPedidos;
    @FXML
    private TableColumn<Pedido,String> pedido_ID;
    @FXML
    private TableColumn<Pedido,String> fecha_pedido;
    @FXML
    private TableColumn<Pedido,String> fecha_entrega;
    @FXML
    private TableColumn<Pedido,String> proveedor_ID;
    @FXML
    private TableColumn<Pedido,String> empleado_ID;
   @FXML
    private TextField txtfpedido;
    @FXML
    private TextField txtfentrega;
    @FXML
    private TextField busqueda;
    @FXML
    private Label fecha;
      @FXML
    private Label nomE;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomE.setText(getVa());
        Calendar calendar= GregorianCalendar.getInstance();
    java.util.Date date=Calendar.getInstance().getTime();
    SimpleDateFormat sdf=new SimpleDateFormat("     dd/MM/yyyy");
    fecha.setText(sdf.format(date));
         ocultar();
         setCenter();
       try {
            String sql = "Select * From pedido";
            Statement st = conexionbd.createStatement();

            ResultSet rs = st.executeQuery(sql);
            pedido_ID.setCellValueFactory(new PropertyValueFactory<>("Pedido_ID"));
            fecha_pedido.setCellValueFactory(new PropertyValueFactory<>("Fecha_Pedida"));
            fecha_entrega.setCellValueFactory(new PropertyValueFactory<>("Fecha_Entrega"));
            proveedor_ID.setCellValueFactory(new PropertyValueFactory<>("Proveedor_ID"));
            empleado_ID.setCellValueFactory(new PropertyValueFactory<>("Empleado_ID"));
            
            celdas(st,rs);
        } catch (SQLException ex) {
           Logger.getLogger(PantallaTablaPedidosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    @FXML
    private void eliminar(MouseEvent event) throws SQLException {
        try{
        Pedido p = (Pedido) tablaPedidos.getSelectionModel().getSelectedItem();
        String eliminar = "DELETE FROM pedido where Pedido_ID='" + p.getPedido_ID()+ "';";

        Statement st = conexionbd.createStatement();
        st.execute(eliminar);

        String show = "select * from pedido";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

         celdas(st,rs);
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    private void ocultar(){
         txtfpedido.setVisible(false);
         txtfentrega.setVisible(false);
         
    }
    private void mostrar(){
         txtfpedido.setVisible(true);
         txtfentrega.setVisible(true);
         
    }
    @FXML
    private void modificar(MouseEvent event) {
        try{
        mostrar();
        Pedido p = tablaPedidos.getSelectionModel().getSelectedItem();
        txtfpedido.setText(String.valueOf(p.getFecha_Pedida()));
        txtfentrega.setText(String.valueOf(p.getFecha_Entrega()));
        
        }catch (Exception e) {
                    ocultar();
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    
    @FXML
    private void actualizar(MouseEvent event) throws SQLException {
        try{
        Pedido p = tablaPedidos.getSelectionModel().getSelectedItem();
        String modify = "update pedido set Fecha_Pedida= '" + txtfpedido.getText() + "' , Fecha_Entrega= '" + txtfentrega.getText() 
                + "' where Pedido_ID like '" + p.getPedido_ID() + "' ; ";

        Statement st = conexionbd.createStatement();
        st.execute(modify);
        String show = "select * from pedido";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

         celdas(st,rs);
            ocultar();
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    
     public void setCenter(){
        busqueda.setPromptText("Ingrese su búsqueda");
        
        ObservableList ob=FXCollections.observableArrayList("Fecha_Pedido","Fecha_Entrega");
        comboxbus.setItems(ob);
        comboxbus.setPromptText("Filtrar");
        comboxbus.setOnAction((l)->{
            if(((String)comboxbus.getValue()).equals("Fecha_Pedido")){
                busqueda.setPromptText("aa-mmm-dd");
            }else if(((String)comboxbus.getValue()).equals("Fecha_Entrega")){
                busqueda.setPromptText("aa-mm-dd");
            }else{
                busqueda.setPromptText("Ingrese su búsqueda");
                
            }
        });
        
        busqueda.textProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue args0,Object o1,Object o2){
                String comboText=(String)comboxbus.getValue();
                if (comboText != null && !comboText.equals("") && !comboText.equals(" ")) {
                try {
                    Statement st = null;
                    ResultSet rs = null;
                    String stbuscar = "";

                        String stringActual = (String) o2;
                        if (((String) comboxbus.getValue()).equals("Fecha_Pedido")) {
                             boolean res=validarFecha(busqueda.getText());
                            if(res==true){
                             stbuscar = "select * from pedido where Fecha_Pedida like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);}else{
                            stbuscar = "select * from venta;"; 
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                celdas(st,rs);
                            }

                        } else if (((String) comboxbus.getValue()).equals("Fecha_Entrega")) {
                             boolean res=validarFecha(busqueda.getText());
                            if(res==true){
                           stbuscar = "select * from pedido where Fecha_Entrega like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);}else{
                            stbuscar = "select * from venta;"; 
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                celdas(st,rs);
                            }
                        }
                       if(busqueda.getText().equals("")){
                       stbuscar = "select * from pedido;"; 
                       st = conexionbd.createStatement();
                       rs = st.executeQuery(stbuscar);
                       celdas(st,rs);}
                    } catch (SQLException ex) {
                         Logger.getLogger(PantallaTablaPedidosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            }
        });
     }
          public static boolean validarFecha(String fecha) {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
     private void celdas(Statement st,ResultSet rs){
       
    try {
        ObservableList<Pedido> datos = FXCollections.observableArrayList();
        while (rs.next()) {
            String id_pedido= rs.getString("Pedido_ID");
            String fecha_ped = rs.getString("Fecha_Pedida");
            String fecha_entrega = rs.getString("Fecha_Entrega");
            String prove_id = rs.getString("Proveedor_ID");
            String empleado_id = rs.getString("Empleado_ID");
            Pedido p1 = new Pedido(Integer.parseInt(id_pedido),Date.valueOf(fecha_ped),Date.valueOf(fecha_entrega),prove_id,Integer.parseInt(empleado_id));
            datos.add(p1);
        }
        
        tablaPedidos.setItems(datos);
        tablaPedidos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    } catch (SQLException ex) {
        Logger.getLogger(PantallaTablaPedidosController.class.getName()).log(Level.SEVERE, null, ex);
    }
        
     }
   @FXML
    private void aggProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProducto.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    
    @FXML
    private void aggCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggCategoria.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    @FXML
    private void aggEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggEmpleado.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();}
            
    @FXML
    private void aggProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProveedor.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }      
            
    @FXML
    private void aggVenta(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggVenta.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show(); 
}
    
    @FXML
    private void aggPedido(MouseEvent event) throws IOException {
     Parent root = FXMLLoader.load(getClass().getResource("PantallaAggPedid.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }     
    
    
    @FXML
    private void aggClientes(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggClientes.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
   
     @FXML
    private void tabProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaTProducto.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCliente(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCliente.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaEmpleados.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCategoria.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaProveedor.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaPedidos.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabVenta(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaVentas.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    @FXML
    private void regreso(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaPrincipal.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
}
