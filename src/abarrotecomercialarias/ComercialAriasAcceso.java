/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import Conexion.conexion;
import java.sql.Connection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Vargas
 */
public class ComercialAriasAcceso extends Application {
    public static Connection conexionbd;
    public static Stage ventPrincipal = new Stage();
    public static Stage ventincio = new Stage();
    public static Scene scene;
    
    @Override
    public void start(Stage stage) throws Exception {
        conexion cn = new conexion();
        conexionbd = cn.getConnection();
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaAcceso.fxml"));
        scene = new Scene(root);
        
        ventPrincipal.setScene(scene);
        ventPrincipal.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
