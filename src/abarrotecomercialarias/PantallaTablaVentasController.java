/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.ventPrincipal;
import static abarrotecomercialarias.ComercialAriasAcceso.ventincio;
import static abarrotecomercialarias.FXMLAccesoController.getVa;
import static abarrotecomercialarias.Mensaje.ventanaEmergente;
import entidades.MontoFecha;
import entidades.Producto;
import entidades.Venta;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaTablaVentasController implements Initializable {
     @FXML
    private Label fechaa;
@FXML
    private Label label;
    @FXML
    private ImageView bproducto;
    @FXML
    private Button producto;
    @FXML
    private Button venta;
    @FXML
    private Button pedido;
    @FXML
    private Button proveedor;
    @FXML
    private Button categoria;
    @FXML
    private Button empleado;
    @FXML
    private ImageView bventa;
    @FXML
    private ImageView vpedido;
    @FXML
    private ImageView bproveedor;
    @FXML
    private ImageView bcategoria;
    @FXML
    private ImageView bempleado;
    @FXML
    private ComboBox<?> comboxbus;
    @FXML
    private ImageView borrartodo;
    @FXML
    private Button categoria1;
    @FXML
    private ImageView bempleado1;
    @FXML
    private TableView<Venta> tablaVentas;
    @FXML
    private TableColumn<Venta,String> Venta_ID;
    @FXML
    private TableColumn<Venta,String> fecha;
    @FXML
    private TableColumn<Venta,String> descuento;
    @FXML
    private TableColumn<Venta,String> monto_final;
    @FXML
    private TableColumn<Venta,String> cliente_ID;
     @FXML
    private TextField txtfecha;
      @FXML
    private TextField busqueda;
        @FXML
      private TableView<MontoFecha> tabmonto;
    @FXML
    private TableColumn<MontoFecha,String> monto;
  @FXML
    private Label nomE;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         nomE.setText(getVa());
        Calendar calendar= GregorianCalendar.getInstance();
    java.util.Date date=Calendar.getInstance().getTime();
    SimpleDateFormat sdf=new SimpleDateFormat("     dd/MM/yyyy");
    fechaa.setText(sdf.format(date));
        ocultar();
        setCenter();
        tabmonto.setVisible(false);
        try {
            String sql = "Select * From venta";
            Statement st = conexionbd.createStatement();

            ResultSet rs = st.executeQuery(sql);
            Venta_ID.setCellValueFactory(new PropertyValueFactory<>("Venta_ID"));
            fecha.setCellValueFactory(new PropertyValueFactory<>("Fecha"));
            descuento.setCellValueFactory(new PropertyValueFactory<>("Descuento"));
            monto_final.setCellValueFactory(new PropertyValueFactory<>("Monto_Final"));
            cliente_ID.setCellValueFactory(new PropertyValueFactory<>("Cliente_ID"));
            
            celdas(st,rs);
        } catch (SQLException ex) {
             Logger.getLogger(PantallaTablaVentasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    @FXML
    private void eliminar(MouseEvent event) throws SQLException {
        try{
        Venta v = (Venta) tablaVentas.getSelectionModel().getSelectedItem();
        String eliminar = "DELETE FROM venta where Venta_ID='" + v.getVenta_ID()+ "';";

        Statement st = conexionbd.createStatement();
        st.execute(eliminar);

        String show = "select * from venta";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

         celdas(st,rs);
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    private void ocultar(){
         txtfecha.setVisible(false);

    }
    private void mostrar(){
         txtfecha.setVisible(true);
    }
    @FXML
    private void modificar(MouseEvent event) {
        try{
        mostrar();
        Venta p = tablaVentas.getSelectionModel().getSelectedItem();
        txtfecha.setText(String.valueOf(p.getFecha())); 
        }catch (Exception e) {
                    ocultar();
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    
    @FXML
    private void actualizar(MouseEvent event) throws SQLException {
        try{
        Venta p = tablaVentas.getSelectionModel().getSelectedItem();
        String modify = "update venta set Fecha = '" + txtfecha.getText() 
                + "' where Venta_ID like '" + p.getVenta_ID() + "' ; ";

        Statement st = conexionbd.createStatement();
        st.execute(modify);
        String show = "select * from venta";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

          celdas(st,rs);
            ocultar();
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
     public void setCenter(){
        busqueda.setPromptText("Ingrese su búsqueda");
        
        ObservableList ob=FXCollections.observableArrayList("Fecha","Monto_Final");
        comboxbus.setItems(ob);
        comboxbus.setPromptText("Filtrar");
        comboxbus.setOnAction((l)->{
            if(((String)comboxbus.getValue()).equals("Fecha")){
                busqueda.setPromptText("aa-mm-dd");
            }else if(((String)comboxbus.getValue()).equals("Monto_Final")){
                busqueda.setPromptText("yyyy");
            }else{
                busqueda.setPromptText("Ingrese su búsqueda");
                
            }
        });
        
        busqueda.textProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue args0,Object o1,Object o2){
                String comboText=(String)comboxbus.getValue();
                boolean verificar=true;
                if (comboText != null && !comboText.equals("") && !comboText.equals(" ")) {
                try {
                    Statement st = null;
                    ResultSet rs = null;
                    String stbuscar = "";

                        String stringActual = (String) o2;
                        if (((String) comboxbus.getValue()).equals("Fecha")) {
                            boolean res=validarFecha(busqueda.getText());
                            if(res==true){
                             stbuscar = "select * from venta where Fecha like " + " \'" + busqueda.getText() + "\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);}else{
                            stbuscar = "select * from venta;"; 
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                celdas(st,rs);
                            }

                        } else if (((String) comboxbus.getValue()).equals("Monto_Final")) {
                           stbuscar = "create view MontoFecha as("
                                   + "select sum(Monto_Final) as monto\n" +
                                        "from venta\n" +
                                        "where year(Fecha)="+" \'" + busqueda.getText() + "\' );";
                                  
                            PreparedStatement pst1 = conexionbd.prepareStatement(stbuscar);
                            pst1.execute();
                            
                            tablaVentas.setVisible(false);
                            tabmonto.setVisible(true);
                            String sql = "Select * From MontoFecha";
                                Statement st1 = conexionbd.createStatement();
                                ResultSet rs1 = st1.executeQuery(sql);
                                if(rs1!=null){
                                monto.setCellValueFactory(new PropertyValueFactory<>("Monto"));
                            ObservableList<MontoFecha> datos = FXCollections.observableArrayList();
        
                             while (rs1.next()) {
                            Double monto= rs1.getDouble("monto");
                            MontoFecha p1 = new MontoFecha(monto);
                            datos.add(p1);
                            }
        
                            tabmonto.setItems(datos);
                            tabmonto.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                            stbuscar = "drop view MontoFecha;";
                                PreparedStatement pst12 = conexionbd.prepareStatement(stbuscar);
                                pst12.execute();
                        }else{ stbuscar = "drop view MontoFecha;";
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                    
                                }}
                       if(busqueda.getText().equals("")){
                       stbuscar = "select * from venta;"; 
                       st = conexionbd.createStatement();
                       rs = st.executeQuery(stbuscar);
                       celdas(st,rs);}
                    } catch (SQLException ex) {
                         Logger.getLogger(PantallaTablaVentasController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
     }
     public static boolean validarFecha(String fecha) throws SQLException {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

     private void celdas(Statement st,ResultSet rs){
         tabmonto.setVisible(false);
        tablaVentas.setVisible(true);
    try {
        ObservableList<Venta> datos = FXCollections.observableArrayList();
        
        while (rs.next()) {
            String id_venta= rs.getString("Venta_ID");
            String fecha = rs.getString("Fecha");
            String descuento = rs.getString("Descuento");
            String monto = rs.getString("Monto_Final");
            String cliente_id = rs.getString("Cliente_ID");
            Venta p1 = new Venta(Integer.parseInt(id_venta),Date.valueOf(fecha),Double.parseDouble(descuento),Double.parseDouble(monto),Integer.parseInt(cliente_id));
            datos.add(p1);
        }
        
        tablaVentas.setItems(datos);
        tablaVentas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    } catch (SQLException ex) {
        Logger.getLogger(PantallaTablaVentasController.class.getName()).log(Level.SEVERE, null, ex);
    }
       
     }
@FXML
    private void aggProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProducto.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    
    @FXML
    private void aggCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggCategoria.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    @FXML
    private void aggEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggEmpleado.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();}
            
    @FXML
    private void aggProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProveedor.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }      
            
    @FXML
    private void aggVenta(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggVenta.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show(); 
}
    
    @FXML
    private void aggPedido(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggPedid.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
   
    @FXML
    private void aggClientes(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggClientes.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
   
     @FXML
    private void tabProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaTProducto.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCliente(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCliente.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaEmpleados.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCategoria.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaProveedor.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaPedidos.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabVenta(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaVentas.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    @FXML
    private void regreso(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaPrincipal.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
}
