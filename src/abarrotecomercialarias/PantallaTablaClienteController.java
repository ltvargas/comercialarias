/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.ventPrincipal;
import static abarrotecomercialarias.ComercialAriasAcceso.ventincio;
import static abarrotecomercialarias.FXMLAccesoController.getVa;
import static abarrotecomercialarias.Mensaje.ventanaEmergente;
import entidades.Cliente;
import entidades.MasCompras;
import entidades.MontoFecha;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaTablaClienteController implements Initializable {
    @FXML
    private Label fecha;
    @FXML
    private Label label;
    @FXML
    private ImageView bproducto;
    @FXML
    private Button producto;
    @FXML
    private Button venta;
    @FXML
    private Button pedido;
    @FXML
    private Button proveedor;
    @FXML
    private Button categoria;
    @FXML
    private Button empleado;
    @FXML
    private ImageView bventa;
    @FXML
    private ImageView vpedido;
    @FXML
    private ImageView bproveedor;
    @FXML
    private ImageView bcategoria;
    @FXML
    private ImageView bempleado;
    @FXML
    private ComboBox<?> comboxbus;
    @FXML
    private ImageView borrartodo;
    @FXML
    private Button categoria1;
    @FXML
    private ImageView bempleado1;
    @FXML
    private TableView<Cliente> tablaCliente;
    @FXML
    private TableColumn<Cliente,String> clienteID;
    @FXML
    private TableColumn<Cliente,String> nombre;
    @FXML
    private TableColumn<Cliente,String> apellido;
    @FXML
    private TableColumn<Cliente,String> direccion;
    @FXML
    private TableColumn<Cliente,String> telefono;
     @FXML
    private TextField txtnombre;
    @FXML
    private TextField txtapellido;
    @FXML
    private TextField txtdirec;
    @FXML
    private TextField txttelf;
    @FXML
    private TextField busqueda;
     @FXML
      private TableView<MasCompras> tabCompras;
    @FXML
    private TableColumn<MasCompras,String> NumeroCompras;
    @FXML
    private TableColumn<MasCompras,String> Nombre;
    @FXML
    private TableColumn<MasCompras,String> Apellido;
      @FXML
    private Label nomE;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            nomE.setText(getVa());
             Calendar calendar= GregorianCalendar.getInstance();
    Date date=Calendar.getInstance().getTime();
    SimpleDateFormat sdf=new SimpleDateFormat("     dd/MM/yyyy");
    fecha.setText(sdf.format(date));   
            ocultar();
            setCenter();
            tabCompras.setVisible(false);
            String sql = "Select * From cliente";
            Statement st = conexionbd.createStatement();

            ResultSet rs = st.executeQuery(sql);
            clienteID.setCellValueFactory(new PropertyValueFactory<>("Cliente_ID"));
            nombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
            apellido.setCellValueFactory(new PropertyValueFactory<>("Apellido"));
            direccion.setCellValueFactory(new PropertyValueFactory<>("Direccion"));
            telefono.setCellValueFactory(new PropertyValueFactory<>("Telefono"));
            celdas(st,rs);
        } catch (SQLException ex) {
            Logger.getLogger(PantallaTablaClienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    @FXML
    private void eliminar(MouseEvent event) throws SQLException {
        try{
        Cliente c = (Cliente) tablaCliente.getSelectionModel().getSelectedItem();
        String eliminar = "DELETE FROM cliente where Cliente_ID='" + c.getCliente_ID()+ "';";

        Statement st = conexionbd.createStatement();
        st.execute(eliminar);

        String show = "select * from cliente";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

         celdas(st,rs);
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    private void ocultar(){
         txtnombre.setVisible(false);
         txtapellido.setVisible(false);
         txtdirec.setVisible(false);
         txttelf.setVisible(false);
    }
    private void mostrar(){
         txtnombre.setVisible(true);
         txtapellido.setVisible(true);
         txtdirec.setVisible(true);
         txttelf.setVisible(true);
    }
    @FXML
    private void modificar(MouseEvent event) {
        try{
        mostrar();
        Cliente p = tablaCliente.getSelectionModel().getSelectedItem();
        txtnombre.setText(String.valueOf(p.getNombre()));
        txtapellido.setText(String.valueOf(p.getApellido()));
        txtdirec.setText(String.valueOf(p.getDireccion()));
        txttelf.setText(String.valueOf(p.getTelefono())); 
         }catch (Exception e) {
                    ocultar();
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    
    @FXML
    private void actualizar(MouseEvent event) throws SQLException {
        try{
        Cliente p = tablaCliente.getSelectionModel().getSelectedItem();
        String modify = "update cliente set nombre = '" + txtnombre.getText() + "' , apellido= '" + txtapellido.getText() + "' , direccion= '" + txtdirec.getText()+ "' , Telefono= '" + txttelf.getText() 
                + "' where Cliente_ID like '" + p.getCliente_ID() + "' ; ";

        Statement st = conexionbd.createStatement();
        st.execute(modify);
        String show = "select * from cliente";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);
        celdas(st,rs);
            ocultar();
             } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    public void setCenter(){
        busqueda.setPromptText("Ingrese su búsqueda");
        
        ObservableList ob=FXCollections.observableArrayList("Nombre","Apellido","realiza más compras");
        comboxbus.setItems(ob);
        comboxbus.setPromptText("Filtrar");
        comboxbus.setOnAction((l)->{
            if(((String)comboxbus.getValue()).equals("Nombre")){
                busqueda.setPromptText("Nombre");
            }else if(((String)comboxbus.getValue()).equals("Apellido")){
                busqueda.setPromptText("Apellido");
            }else if(((String)comboxbus.getValue()).equals("realiza más compras")){
                busqueda.setPromptText("yyyy");
            }else{
                busqueda.setPromptText("Ingrese su búsqueda");
                
            }
        });
        
        busqueda.textProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue args0,Object o1,Object o2){
                String comboText=(String)comboxbus.getValue();
                if (comboText != null && !comboText.equals("") && !comboText.equals(" ")) {
                try {
                    Statement st = null;
                    ResultSet rs = null;
                    String stbuscar = "";

                        String stringActual = (String) o2;
                        if (((String) comboxbus.getValue()).equals("Nombre")) {
                             stbuscar = "select * from cliente where nombre like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);

                        } else if (((String) comboxbus.getValue()).equals("Apellido")) {
                           stbuscar = "select * from cliente where apellido like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);
                        }else if (((String) comboxbus.getValue()).equals("realiza más compras")) {
                           stbuscar = "create view MasCompras as(\n" +
                                        "select count(c.cliente_ID) as Compras,c.nombre,c.apellido\n" +
                                        "from venta as v\n" +
                                        "join cliente as c on v.cliente_ID=c.cliente_ID\n" +
                                        "where year(fecha)="+" \'" + busqueda.getText() + "\'  \n" +
                                        "group by c.nombre\n" +
                                        "order by Compras desc\n" +
                                        "limit 5);";
                                  
                            PreparedStatement pst1 = conexionbd.prepareStatement(stbuscar);
                            pst1.execute();
                            
                             tablaCliente.setVisible(false);
                             tabCompras.setVisible(true);
                            String sql = "Select * From MasCompras";
                                Statement st1 = conexionbd.createStatement();
                                ResultSet rs1 = st1.executeQuery(sql);
                                if(rs1!=null){
                                NumeroCompras.setCellValueFactory(new PropertyValueFactory<>("NumeroCompras"));
                                Nombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
                                Apellido.setCellValueFactory(new PropertyValueFactory<>("Apellido"));
                            ObservableList<MasCompras> datos = FXCollections.observableArrayList();
        
                             while (rs1.next()) {
                            Integer n= rs1.getInt("Compras");
                            String nombre= rs1.getString("nombre");
                            String apellido= rs1.getString("apellido");
                            MasCompras p1 = new MasCompras(n,nombre,apellido);
                            datos.add(p1);
                            }
        
                            tabCompras.setItems(datos);
                            tabCompras.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                            stbuscar = "drop view MasCompras;";
                                PreparedStatement pst12 = conexionbd.prepareStatement(stbuscar);
                                pst12.execute();
                        }else{ stbuscar = "drop view MasCompras;";
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                    
                                }}
                       if(busqueda.getText().equals("")){
                       stbuscar = "select * from cliente;"; 
                       st = conexionbd.createStatement();
                       rs = st.executeQuery(stbuscar);
                       celdas(st,rs);}
                    } catch (SQLException ex) {
                        Logger.getLogger(PantallaTablaClienteController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            }
        });
     }
     private void celdas(Statement st,ResultSet rs){
          tabCompras.setVisible(false);
        tablaCliente.setVisible(true);
        try {
            ObservableList<Cliente> datos = FXCollections.observableArrayList();

        while (rs.next()) {
                   String id_cliente= rs.getString("Cliente_ID");
                    String nombre = rs.getString("Nombre");
                    String apellido = rs.getString("Apellido");
                    String direccion = rs.getString("Direccion");
                    String telefono = rs.getString("Telefono");
                    Cliente p1 = new Cliente(Integer.parseInt(id_cliente),nombre,apellido,direccion,telefono);
                    datos.add(p1);
            }

            tablaCliente.setItems(datos);
            tablaCliente.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        } catch (SQLException ex) {
            Logger.getLogger(PantallaTablaClienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
    
    @FXML
    private void aggProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProducto.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    
    @FXML
    private void aggCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggCategoria.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    @FXML
    private void aggEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggEmpleado.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();}
            
    @FXML
    private void aggProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProveedor.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }      
            
    @FXML
    private void aggVenta(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggVenta.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show(); 
}
    
    @FXML
    private void aggPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggPedid.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    @FXML
    private void aggClientes(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggClientes.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
   
     @FXML
    private void tabProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaTProducto.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCliente(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCliente.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaEmpleados.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCategoria.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaProveedor.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaPedidos.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabVenta(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaVentas.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    @FXML
    private void regreso(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaPrincipal.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
}

    
