/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.ventPrincipal;
import static abarrotecomercialarias.ComercialAriasAcceso.ventincio;
import static abarrotecomercialarias.FXMLAccesoController.getVa;
import static abarrotecomercialarias.Mensaje.ventanaEmergente;
import entidades.Categoria;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaTablaCategoriaController implements Initializable {
    @FXML
    private Label fecha;
    @FXML
    private Label label;
    @FXML
    private ImageView bproducto;
    @FXML
    private Button producto;
    @FXML
    private Button venta;
    @FXML
    private Button pedido;
    @FXML
    private Button proveedor;
    @FXML
    private Button categoria;
    @FXML
    private Button empleado;
    @FXML
    private ImageView bventa;
    @FXML
    private ImageView vpedido;
    @FXML
    private ImageView bproveedor;
    @FXML
    private ImageView bcategoria;
    @FXML
    private ImageView bempleado;
    @FXML
    private ComboBox<?> comboxbus;
    @FXML
    private ImageView borrartodo;
    @FXML
    private Button categoria1;
    @FXML
    private ImageView bempleado1;
         @FXML
    private Label nomE;
    @FXML
    private TableView<Categoria> tablaCategorias;
    @FXML
    private TableColumn<Categoria,String> Categoria_ID;
    @FXML
    private TableColumn<Categoria,String> Categoriaa;
    @FXML
    private TableColumn<Categoria,String> descripcion;
    @FXML
    private TextField txtcategoria;
    @FXML
    private TextField txtdescrip;
    @FXML
    private TextField busqueda;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomE.setText(getVa());
          Calendar calendar= GregorianCalendar.getInstance();
    Date date=Calendar.getInstance().getTime();
    SimpleDateFormat sdf=new SimpleDateFormat("     dd/MM/yyyy");
    fecha.setText(sdf.format(date));
        ocultar();
        setCenter();
        try {
            String sql = "Select * From categoria";
            Statement st = conexionbd.createStatement();
            ResultSet rs = st.executeQuery(sql);
            Categoria_ID.setCellValueFactory(new PropertyValueFactory<>("Categoria_ID"));
            Categoriaa .setCellValueFactory(new PropertyValueFactory<>("Categoria"));
            descripcion.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));
            celdas(st,rs);
        } catch (SQLException ex) {
             Logger.getLogger(PantallaTablaCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    @FXML
    private void eliminar(MouseEvent event) throws SQLException {
        try{
        Categoria c = (Categoria) tablaCategorias.getSelectionModel().getSelectedItem();
        String eliminar = "DELETE FROM categoria where Categoria_ID='" + c.getCategoria_ID()+ "';";

        Statement st = conexionbd.createStatement();
        st.execute(eliminar);

        String show = "select * from categoria";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

        celdas(st,rs);
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    private void ocultar(){
         txtcategoria.setVisible(false);
         txtdescrip.setVisible(false);
    }
    private void mostrar(){
         txtcategoria.setVisible(true);
         txtdescrip.setVisible(true);
    }
    @FXML
    private void modificar(MouseEvent event) {
        try{
        mostrar();
        Categoria p = tablaCategorias.getSelectionModel().getSelectedItem();
        txtcategoria.setText(String.valueOf(p.getCategoria()));
        txtdescrip.setText(String.valueOf(p.getDescripcion()));
         }catch (Exception e) {
                    ocultar();
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    
    @FXML
    private void actualizar(MouseEvent event) throws SQLException {
        try{
        Categoria p = tablaCategorias.getSelectionModel().getSelectedItem();
        String modify = "update categoria set Categoria = '" + txtcategoria.getText() + "' ,Descripcion= '" + txtdescrip.getText() 
                + "' where Categoria_ID like '" + p.getCategoria_ID() + "' ; ";

        Statement st = conexionbd.createStatement();
        st.execute(modify);
        String show = "select * from categoria";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

        celdas(st,rs);
            ocultar();
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    public void setCenter(){
        busqueda.setPromptText("Ingrese su búsqueda");
        
        ObservableList ob=FXCollections.observableArrayList("Categoria");
        comboxbus.setItems(ob);
        comboxbus.setPromptText("Filtrar");
        comboxbus.setOnAction((l)->{
            if(((String)comboxbus.getValue()).equals("Categoria")){
                busqueda.setPromptText("Categoria");
            }else{
                busqueda.setPromptText("Ingrese su búsqueda");
                
            }
        });
        
        busqueda.textProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue args0,Object o1,Object o2){
                String comboText=(String)comboxbus.getValue();
                if (comboText != null && !comboText.equals("") && !comboText.equals(" ")) {
                try {
                    Statement st = null;
                    ResultSet rs = null;
                    String stbuscar = "";

                        String stringActual = (String) o2;
                        if (((String) comboxbus.getValue()).equals("Categoria")) {
                             stbuscar = "select * from categoria where Categoria like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);

                        } 
                       if(busqueda.getText().equals("")){
                       stbuscar = "select * from categoria;"; 
                       st = conexionbd.createStatement();
                       rs = st.executeQuery(stbuscar);
                       celdas(st,rs);}
                    } catch (SQLException ex) {
                            Logger.getLogger(PantallaTablaCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
                        }
            }
            }
        });
     }
     private void celdas(Statement st,ResultSet rs){
        try {
            ObservableList<Categoria> datos = FXCollections.observableArrayList();

        while (rs.next()) {
                    String id_cate= rs.getString("Categoria_ID");
                    String categoria = rs.getString("Categoria");
                    String descripcion = rs.getString("Descripcion");
                    Categoria c1 = new Categoria(Integer.parseInt(id_cate),categoria,descripcion);
                    datos.add(c1);
                    }

            tablaCategorias.setItems(datos);
            tablaCategorias.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        } catch (SQLException ex) {
           Logger.getLogger(PantallaTablaCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
           }
     }
    
    @FXML
    private void aggProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProducto.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    
    @FXML
    private void aggCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggCategoria.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    @FXML
    private void aggEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggEmpleado.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();}
            
    @FXML
    private void aggProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProveedor.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }      
            
    @FXML
    private void aggVenta(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggVenta.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show(); 
}
    
    @FXML
    private void aggPedido(MouseEvent event) throws IOException {
   Parent root = FXMLLoader.load(getClass().getResource("PantallaAggPedid.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    @FXML
    private void aggClientes(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggClientes.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
   
     @FXML
    private void tabProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaTProducto.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCliente(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCliente.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaEmpleados.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCategoria.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaProveedor.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaPedidos.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabVenta(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaVentas.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    @FXML
    private void regreso(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaPrincipal.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
   
    }    
    

