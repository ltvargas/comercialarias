/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import entidades.DetallePedido;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaAggPedidController implements Initializable {
PreparedStatement ps;
    @FXML
    private TextField txtPedido_id;
    @FXML
    private TextField txtnombre_proveedor;
    @FXML
    private TextField txt_cliente;
    @FXML
    private TextField txtProducto_id;
 
    @FXML
    private TextField txtPrecio;
    @FXML
    private TextField txt_emp;
    @FXML
    private ComboBox combo_Prod;
    @FXML
    private TextField txtcantidad;
    @FXML
    private TextField fechaselec;
    @FXML
    private TextField txtIVA;
    @FXML
    private TextField txtTotal;
    @FXML
    private TableView<DetallePedido> tablefactura;
    @FXML
    private TableColumn<DetallePedido,String> Id_Producto;
    @FXML
   private TableColumn<DetallePedido,String> CantPro;
    @FXML
    private TableColumn<DetallePedido,String> Precio_Unitario;
    @FXML
    private TableColumn<DetallePedido,String> Total;
    @FXML
    private DatePicker fechaselecentrega;
     private List<DetallePedido> detalle=new ArrayList<>();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    Calendar calendar= GregorianCalendar.getInstance();
    Date date=Calendar.getInstance().getTime();
    SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
        fechaselec.setText(sdf.format(date));
        txtPedido_id.setText(Integer.toString(numero_pedido()));
        Object[] idarticulo = poblar_combox("producto","Nombre");
        combo_Prod.getItems().removeAll(idarticulo);
        for(int i=0;i<idarticulo.length;i++)
        {
        combo_Prod.getItems().add(idarticulo[i]);
        }}
     
    
    public int numero_pedido(){
        int numero;
        int numero_actual=0;
        try {
            String ss = "SELECT count(*) as contador FROM pedido";
            Statement ste =conexionbd.createStatement();
            ResultSet rs = ste.executeQuery(ss);
            rs.next();
            numero = rs.getInt("contador");
            rs.close();
            numero_actual=numero+1;           
        } catch (SQLException ex) {
            Logger.getLogger(PantallaAggVentaController.class.getName()).log(Level.SEVERE, null, ex);
        } return numero_actual;
    }   

     @FXML
    private void BuscarCliente(MouseEvent event) {
         String[] columnas={"Nombre"};
            Object[][] resultado = GetTabla(columnas, "proveedor","select Nombre from proveedor where Proveedor_ID='"+ txt_cliente.getText()+"';");
            System.out.println(resultado[0][0]);
            txtnombre_proveedor.setText(resultado[0][0].toString());
    }
    
    public Object [][] GetTabla(String colName[], String tabla,String sqll){
        int registros = 0;      
      try{
          String sql = "SELECT count(*) as total FROM " + tabla;
          Statement st = conexionbd.createStatement();
          ResultSet rs = st.executeQuery(sql);
          rs.next();
          registros = rs.getInt("total");
          rs.close();
          }catch(SQLException e){
             System.out.println(e);
          }

    Object[][] data = new String[registros][colName.length];
    String col[] = new String[colName.length];
    
      try{
        Statement st = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(sqll);  
        int i = 0;
        while(rs.next()){
            for(int j=0; j<=colName.length-1;j++){
                col[j] = rs.getString(colName[j]);
                data[i][j] = col[j];
            }
            i++;
         }
         rs.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }
      
      public Object[] poblar_combox(String tabla, String nombrecol){
      int registros = 0;      
      try{
          String sql = "SELECT count(*) as total FROM " + tabla;
          Statement st = conexionbd.createStatement();
          ResultSet rs = st.executeQuery(sql);
          rs.next();
          registros = rs.getInt("total");
          rs.close();
          }catch(SQLException e){
             System.out.println(e);
          }

    Object[] datos = new Object[registros];
      try{
         Statement st = conexionbd.createStatement();
         ResultSet rs = st.executeQuery("SELECT Nombre FROM producto");
         int i = 0;
         while(rs.next()){
            datos[i] = rs.getObject(nombrecol);
            i++;
         }
         rs.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return datos;
 }

    @FXML
    private void AccionCombo(ActionEvent event) {
        if(combo_Prod.getValue()!=null )
     {
            String[] columnas={"Producto_ID","Precio_Venta"};
            Object[][] resultado = GetTabla(columnas, "producto","select Producto_ID, Precio_Venta from producto where Nombre='"+ combo_Prod.getValue().toString() +"';");
            System.out.println(resultado[0][0]);
            txtProducto_id.setText(resultado[0][0].toString());
            txtPrecio.setText(resultado[0][1].toString()); 
        } }      
      
    
    public Double datos_totalfactura(){
    double data =0;
      try{   
         ps= conexionbd.prepareStatement("select sum(subtotal) as total\n" +
            "from productoxdetalle as a\n" +
            "join producto as b on b.Nombre=a.Nombre;");
         ResultSet res = ps.executeQuery();
         while(res.next()){
            data = res.getDouble("total");
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
    }
    
    @FXML
    private void btnRegistrarDetalle(MouseEvent event) {
                try {
            String sql = "create  table  IF NOT EXISTS productoxdetalle (\n" +
                            "Nombre varchar(40),\n" +
                            "Unidades INT ,\n" +
                            "Precio_Unidad double,\n" +
                            "Subtotal double\n" +
                            ");";
            PreparedStatement pst1 = conexionbd.prepareStatement(sql);
                            pst1.execute();
            
            if(!txtcantidad.getText().equals("0")){
                    int cant=Integer.parseInt(txtcantidad.getText());
                    double precio=Double.parseDouble(txtPrecio.getText());
                    double valor=cant*precio;
                    String sqlp1 = "select a.Nombre\n" +
                                    "from producto as a where a.Producto_ID="+txtProducto_id.getText()+";";
                    Statement sttn = conexionbd.createStatement();
                    ResultSet rsnom = sttn.executeQuery(sqlp1);
                    String Nompro="";
                    if(rsnom.next()){
                        Nompro =rsnom.getString("Nombre");
                    }

                    String sqlp = "insert into productoxdetalle values('"+Nompro+"',"+txtcantidad.getText()+","+txtPrecio.getText()+","+valor+");";
                    PreparedStatement pst = conexionbd.prepareStatement(sqlp);
                            pst.execute();
                    
                    detalle.add(new DetallePedido(Integer.parseInt(txtProducto_id.getText()),Nompro,Integer.parseInt(txtcantidad.getText()),Double.parseDouble(txtPrecio.getText()),valor));
                     String sqlll ="select * from productoxdetalle;";
                   
                    Statement stt = conexionbd.createStatement();
                    ResultSet rss = stt.executeQuery(sqlll);
                    Precio_Unitario.setCellValueFactory(new PropertyValueFactory<>("Precio_Unitario"));
                    Id_Producto.setCellValueFactory(new PropertyValueFactory<>("Nombre_Producto"));
                    CantPro.setCellValueFactory(new PropertyValueFactory<>("Unidades"));
                    Total.setCellValueFactory(new PropertyValueFactory<>("Subtotal"));
             
                    ObservableList<DetallePedido> datos = FXCollections.observableArrayList();
           
            while (rss.next()) {

                    String Nombre = rss.getString("Nombre");
                    String uni = rss.getString("Unidades");
                    String preciou = rss.getString("Precio_Unidad");
                    String sub = rss.getString("Subtotal");
                    DetallePedido dv = new DetallePedido(Integer.parseInt(txtProducto_id.getText()),Nombre,Integer.parseInt(uni),Double.parseDouble(preciou),Double.parseDouble(sub));
                    datos.add(dv);
                
            }
            
            tablefactura.setItems(datos);
            tablefactura.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                }
            else
            {
                JOptionPane.showMessageDialog(null, "La cantidad no es valida");
            }

        Double subbb=datos_totalfactura();
        txtTotal.setText(Double.toString(subbb));

        } catch (SQLException ex) {
            Logger.getLogger(PantallaAggVentaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }                           

    @FXML
    private void btnGenerarFactura(MouseEvent event) throws SQLException {
        Date dd = new java.util.Date(); 
        Date date3 = new java.sql.Date(dd.getTime());
        System.out.println(date3);
        String linea = "Insert Into pedido (Fecha_Pedida,Fecha_Entrega,Proveedor_ID,Empleado_ID) values ('"+ date3 + "\',\'" + fechaselecentrega.getValue() + "\',\'" + txt_cliente.getText() + "\',\'" + txt_emp.getText() + "\'" + ");";
        PreparedStatement pst = conexionbd.prepareStatement(linea);
        pst.execute();
        
        for(DetallePedido d:detalle){
           
        String linea2 = "call CrearDetalle_Pedido("+d.getUnidades()+","+d.getPrecio_Unitario()+","+d.getSubtotal()+","+d.getProducto_ID()+","+txtPedido_id.getText()+",@result)";
        PreparedStatement pst2 = conexionbd.prepareStatement(linea2);
        pst2.execute();
        }
        String linea3 = "drop table if exists productoxdetalle;";                
        PreparedStatement pst3 = conexionbd.prepareStatement(linea3);
        pst3.execute();
        JOptionPane.showMessageDialog(null,"Pedido guardado exitosamente.");

    }
        
    }
    

