/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.FXMLAccesoController.getVa;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaAggCategoriaController implements Initializable {
    
     @FXML
    private Label lblnom_cat;
     @FXML
    private TextField txtnom_cat;
      @FXML
    private Label lbldesc_cat;
     @FXML
    private TextArea txtArea_desCat ;
    @FXML
    private Button btnvolver;
     @FXML
    private Button btnguardar;
   
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }    

    @FXML
    private void btnVolver(MouseEvent event) throws IOException {
     }   
    


    @FXML
    private void btnGuardar(MouseEvent event) throws SQLException {
        String linea = "Insert Into categoria (categoria,descripcion) values ('" + txtnom_cat.getText() + "\',\'" + txtArea_desCat.getText() + "\'" + ");";
        PreparedStatement pst = conexionbd.prepareStatement(linea);
        pst.execute();
        JOptionPane.showMessageDialog(null,"Categoría guardada exitosamente.");
        txtnom_cat.setText("");
        txtArea_desCat.setText("");
     
        
    }
    
}
