/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import entidades.Empleado;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaAggEmpleadoController implements Initializable {
    @FXML
    private Label lblempleado_id;
     @FXML
    private TextField txtEmp_id;
     @FXML
    private Label lbl_nomEmp;
     @FXML
    private TextField txtNom_emp ;
      @FXML
    private Label lblApe_emp;
      @FXML
    private TextField txtApe_Emp ; 
       @FXML
    private Label lbl_FechaNac ;
      @FXML
    private TextField txtfecha_nac ; 
    @FXML
    private Label lbl_tlfEmp ;
      @FXML
    private TextField txtTlf_emp ; 
    @FXML
    private Button btnvolver;
     @FXML
    private Button btnguardar;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public void limpiar()
    {
       txtEmp_id.setText("");
       txtNom_emp.setText("");
       txtApe_Emp.setText("");
       txtfecha_nac.setText("");
       txtTlf_emp.setText("");
       
    }
    
    
    @FXML
    private void btnGuardar_Empleado(MouseEvent event)throws SQLException {
       String linea = "Insert Into empleado (Empleado_ID,nombre,apellido,Fecha_Nacimiento,Telefono) values ('"+ txtEmp_id.getText()+ "\',\'"  + txtNom_emp.getText() + "\',\'" + txtApe_Emp.getText() + "\',\'" +  txtfecha_nac.getText()+"\',\'" + txtTlf_emp.getText() + "\'" + ");";
        PreparedStatement pst = conexionbd.prepareStatement(linea);
        pst.execute();
        JOptionPane.showMessageDialog(null,"Empleado guardado exitosamente.");
        txtEmp_id.setText("");
        txtNom_emp.setText("");
        txtApe_Emp.setText("");
        txtfecha_nac.setText("");
        txtTlf_emp.setText("");
    }

    @FXML
    private void btnVolver(MouseEvent event) throws IOException {
           }   
    
    
}
