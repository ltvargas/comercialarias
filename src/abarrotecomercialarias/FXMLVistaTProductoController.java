/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.ventPrincipal;
import static abarrotecomercialarias.ComercialAriasAcceso.ventincio;
import static abarrotecomercialarias.FXMLAccesoController.getVa;
import static abarrotecomercialarias.Mensaje.ventanaEmergente;
import entidades.Producto;
import entidades.ProductoNoVporfecha;
import entidades.ProductoVporfecha;
import entidades.ProductoporCategoria;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TASHZ
 */
public class FXMLVistaTProductoController implements Initializable {
    @FXML
    private Label nomE;
    @FXML
    private Label fecha;
    @FXML
    private Label label;
    @FXML
    private ImageView bproducto;
    @FXML
    private Button producto;
    @FXML
    private Button venta;
    @FXML
    private Button pedido;
    @FXML
    private Button proveedor;
    @FXML
    private Button categoria;
    @FXML
    private Button empleado;
    @FXML
    private ImageView bventa;
    @FXML
    private ImageView vpedido;
    @FXML
    private ImageView bproveedor;
    @FXML
    private ImageView bcategoria;
    @FXML
    private ImageView bempleado;
    @FXML
    private ComboBox<?> comboxbus;
    @FXML
    private ImageView borrartodo;
    @FXML
    private Button categoria1;
    @FXML
    private ImageView bempleado1;
   @FXML
    private TableView<Producto> tablaProductos;
    @FXML
    private TableColumn<Producto,String> Producto_ID;
    @FXML
    private TableColumn<Producto, String> nombre;
    @FXML
    private TableColumn<Producto, String> marca;
    @FXML
    private TableColumn<Producto, String> stock;
    @FXML
    private TableColumn<Producto, String> precio_Venta;
    @FXML
    private TableColumn<Producto, String> ccategoria;
    @FXML
    private TableColumn<Producto, String> Proveedor_ID;
     @FXML
    private TextField txtnombre;
    @FXML
    private TextField txtstock;
    @FXML
    private TextField txtmarca;
    @FXML
    private TextField txtprecio;
    @FXML
    private TextField busqueda;
    @FXML
    private TableView<ProductoporCategoria> tabproductosvendidos;
    @FXML
    private TableColumn<ProductoporCategoria,String> ProductosVendidos;
    @FXML
    private TableColumn<ProductoporCategoria, String> Categoria;
    @FXML
    private TableView<ProductoVporfecha> tabprodVxFecha;
    @FXML
    private TableColumn<ProductoVporfecha,String> ProductosV1;
    @FXML
    private TableColumn<ProductoVporfecha, String> Preciouni;
    @FXML
    private TableColumn<ProductoVporfecha, String> cantv;
    @FXML
    private TableColumn<ProductoVporfecha, String> Ganancia;
    @FXML
    private TableView<ProductoNoVporfecha> tabprodNoVxFecha;
    @FXML
    private TableColumn<ProductoNoVporfecha,String> ProductosV11;
    @FXML
    private TableColumn<ProductoNoVporfecha, String> Marca11;
    @FXML
    private TableColumn<ProductoNoVporfecha, String> Stock11;
    @FXML
    private TableColumn<ProductoNoVporfecha, String>Precio_Unitario11;
    
    /**
     * Initializes the controller class.
     */
   
   
        
     @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomE.setText(getVa());
          Calendar calendar= GregorianCalendar.getInstance();
    Date date=Calendar.getInstance().getTime();
    SimpleDateFormat sdf=new SimpleDateFormat("     dd/MM/yyyy");
    fecha.setText(sdf.format(date));
        ocultar();
        setCenter();
        tabproductosvendidos.setVisible(false);
        tabprodVxFecha.setVisible(false);
         tabprodNoVxFecha.setVisible(false);
        try {
            String sql = "Select * From producto";
            Statement st = conexionbd.createStatement();

            ResultSet rs = st.executeQuery(sql);
            Producto_ID.setCellValueFactory(new PropertyValueFactory<>("Producto_ID"));
            nombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
            marca.setCellValueFactory(new PropertyValueFactory<>("Marca"));
            stock.setCellValueFactory(new PropertyValueFactory<>("Stock"));
            precio_Venta.setCellValueFactory(new PropertyValueFactory<>("Precio_Venta"));
            ccategoria.setCellValueFactory(new PropertyValueFactory<>("Categoria_ID"));
            Proveedor_ID.setCellValueFactory(new PropertyValueFactory<>("Proveedor_ID"));
           celdas(st,rs);
        } catch (SQLException ex) {
            Logger.getLogger(FXMLVistaTProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    private void ocultar(){
         txtnombre.setVisible(false);
         txtmarca.setVisible(false);
         txtstock.setVisible(false);
         txtprecio.setVisible(false);
    }
    private void mostrar(){
         txtnombre.setVisible(true);
         txtmarca.setVisible(true);
         txtstock.setVisible(true);
         txtprecio.setVisible(true);
    }
    @FXML
    private void modificar(MouseEvent event) {
        try{
        mostrar();
        Producto p = tablaProductos.getSelectionModel().getSelectedItem();
        txtnombre.setText(String.valueOf(p.getNombre()));
        txtmarca.setText(String.valueOf(p.getMarca()));
        txtstock.setText(String.valueOf(p.getStock()));
        txtprecio.setText(String.valueOf(p.getPrecio_Venta()));  
        }catch (Exception e) {
                    ocultar();
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
    
    @FXML
    private void actualizar(MouseEvent event) throws SQLException {
        try{
        Producto p = tablaProductos.getSelectionModel().getSelectedItem();
        String modify = "update producto set Nombre = '" + txtnombre.getText() + "' , Marca = '" + txtmarca.getText() + "' , Stock = '" + txtstock.getText()+ "' , Precio_Venta = '" + txtprecio.getText() 
                + "' where Producto_ID like '" + p.getProducto_ID() + "' ; ";

        Statement st = conexionbd.createStatement();
        st.execute(modify);
        String show = "select * from producto";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);
        celdas(st,rs);
            ocultar();
            } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }

     public void setCenter(){
        busqueda.setPromptText("Ingrese su búsqueda");
        
        ObservableList ob=FXCollections.observableArrayList("Nombre","Marca","Vendidos","No_Vendidos","ProductoVendido_Categoria");
        comboxbus.setItems(ob);
        comboxbus.setPromptText("Filtrar");
        comboxbus.setOnAction((l)->{
            if(((String)comboxbus.getValue()).equals("Nombre")){
                busqueda.setPromptText("Nombre");
            }else if(((String)comboxbus.getValue()).equals("Marca")){
                busqueda.setPromptText("Marca");
            }else if(((String)comboxbus.getValue()).equals("Vendidos")){
                busqueda.setPromptText("yyyy-MM-dd");
            }else if(((String)comboxbus.getValue()).equals("No_Vendidos")){
                busqueda.setPromptText("yyyy-MM-dd/yyyy-MM-dd");
            }else if(((String)comboxbus.getValue()).equals("ProductoVendido_Categoria")){
                busqueda.setPromptText("Categoria");
            }else{
                busqueda.setPromptText("Ingrese su búsqueda");
                
            }
        });
        
        busqueda.textProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue args0,Object o1,Object o2){
                String comboText=(String)comboxbus.getValue();
                if (comboText != null && !comboText.equals("") && !comboText.equals(" ")) {
                try {
                    Statement st = null;
                    ResultSet rs = null;
                    String stbuscar = "";

                        String stringActual = (String) o2;
                        if (((String) comboxbus.getValue()).equals("Nombre")) {
                             stbuscar = "select * from producto where Nombre like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);

                        } else if (((String) comboxbus.getValue()).equals("Marca")) {
                           stbuscar = "select * from producto where Marca like " + " \'" + busqueda.getText() + "%\' ;";
                            st = conexionbd.createStatement();
                            rs = st.executeQuery(stbuscar);
                            celdas(st,rs);
                        }
                        else if (((String) comboxbus.getValue()).equals("Vendidos")){
                            boolean res=validarFecha(busqueda.getText());
                            if(res==true){
                            stbuscar ="create view ProductosVendidosXfecha as(\n" +
                                        "select a.Nombre,a.Precio_Venta,sum(b.Unidades) as cantidadVendida,b.Unidades*a.Precio_Venta as Ganancia\n" +
                                        "from producto as a\n" +
                                        "join detalle_venta as b on a.producto_ID=b.Producto_ID\n" +
                                        "join Venta on b.Venta_ID=Venta.Venta_ID\n" +
                                        "where Fecha like " + " \'" + busqueda.getText() + "\'\n" +
                                        "group by(b.Producto_ID));";
                                    
                           
                            PreparedStatement pst1 = conexionbd.prepareStatement(stbuscar);
                            pst1.execute();
                            
                            tablaProductos.setVisible(false);
                             tabprodVxFecha.setVisible(true);
                            String sql = "Select * from ProductosVendidosXfecha";
                                Statement st1 = conexionbd.createStatement();
                                ResultSet rs1 = st1.executeQuery(sql);
                                if(rs1!=null){
                                ProductosV1.setCellValueFactory(new PropertyValueFactory<>("Producto"));
                                 Preciouni.setCellValueFactory(new PropertyValueFactory<>("Precio_Unitario"));
                                 cantv.setCellValueFactory(new PropertyValueFactory<>("Cantidad_vendida"));
                                 Ganancia.setCellValueFactory(new PropertyValueFactory<>("Ganancia"));
                            ObservableList<ProductoVporfecha> datos = FXCollections.observableArrayList();
        
                             while (rs1.next()) {
                            String nom= rs1.getString("Nombre");
                            String prev= rs1.getString("Precio_Venta");
                            String canv= rs1.getString("cantidadVendida");
                            String gana= rs1.getString("Ganancia");
                            ProductoVporfecha p1 = new ProductoVporfecha(nom,Double.parseDouble(prev),Integer.parseInt(canv),Double.parseDouble(gana));
                            datos.add(p1);
                            }
        
                           tabprodVxFecha.setItems(datos);
                            tabprodVxFecha.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                            stbuscar = "drop view ProductosVendidosXfecha;";
                                PreparedStatement pst12 = conexionbd.prepareStatement(stbuscar);
                                pst12.execute();
                        }else{ stbuscar = "drop view ProductosVendidosXfecha;";
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                    
                                }}
                        }else if (((String) comboxbus.getValue()).equals("No_Vendidos")){
                            String[] fecha=busqueda.getText().split("/");
                            if(fecha.length==2){
                            boolean res=validarFecha(fecha[0]);
                            boolean res1=validarFecha(fecha[1]);
                            if(res==true && res1==true){
                            stbuscar ="create view ProductosNoVendidosXfechas as(\n" +
                                      " select a.Nombre,a.Marca,a.Stock,a.Precio_Venta \n" +
                                        "from producto as a\n" +
                                        "where a.Producto_ID NOT IN(select d.Producto_ID from detalle_venta as d\n" +
                                        "join venta on d.Venta_ID=d.DetalleV_ID\n" +
                                        "where Fecha>'"+fecha[0]+"' and Fecha<'"+fecha[1]+"'));";
                            PreparedStatement pst1 = conexionbd.prepareStatement(stbuscar);
                            pst1.execute();
                            
                            tablaProductos.setVisible(false);
                             tabprodNoVxFecha.setVisible(true);
                            String sql = "Select * from ProductosNoVendidosXfechas";
                                Statement st1 = conexionbd.createStatement();
                                ResultSet rs1 = st1.executeQuery(sql);
                                if(rs1!=null){
                                ProductosV11.setCellValueFactory(new PropertyValueFactory<>("Producto"));
                                Marca11.setCellValueFactory(new PropertyValueFactory<>("Marca"));
                                Stock11.setCellValueFactory(new PropertyValueFactory<>("Stock"));
                                Precio_Unitario11.setCellValueFactory(new PropertyValueFactory<>("Precio_Unitario"));
                            ObservableList<ProductoNoVporfecha> datos = FXCollections.observableArrayList();
        
                             while (rs1.next()) {
                            String nom= rs1.getString("Nombre");
                            String prev= rs1.getString("Precio_Venta");
                            String marca= rs1.getString("Marca");
                            String stock= rs1.getString("Stock");
                            ProductoNoVporfecha p1 = new ProductoNoVporfecha(nom,marca,Integer.parseInt(stock),Double.parseDouble(prev));
                            datos.add(p1);
                            }
        
                           tabprodNoVxFecha.setItems(datos);
                           tabprodNoVxFecha.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                            stbuscar = "drop view ProductosNoVendidosXfechas;";
                                PreparedStatement pst12 = conexionbd.prepareStatement(stbuscar);
                                pst12.execute();
                        }else{ stbuscar = "drop view ProductosNoVendidosXfechas;";
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                    
                                }}
                        }}else if (((String) comboxbus.getValue()).equals("ProductoVendido_Categoria")) {
                           stbuscar = "create view ProductoVendido_Categoria as(\n" +
                                        "select count(a.producto_ID) as ProductosVendidos, c.categoria\n" +
                                        "from producto as a\n" +
                                        "join categoria as c on a.categoria_ID=c.categoria_ID\n" +
                                        "join detalle_venta as d on d.producto_ID=a.Producto_ID\n" +
                                        "join Venta on d.Venta_ID=Venta.Venta_ID\n" +
                                        "group by c.categoria);";    
                            PreparedStatement pst1 = conexionbd.prepareStatement(stbuscar);
                            pst1.execute();
                            
                            tablaProductos.setVisible(false);
                            tabproductosvendidos.setVisible(true);
                            String sql = "Select * From ProductoVendido_Categoria";
                                Statement st1 = conexionbd.createStatement();
                                ResultSet rs1 = st1.executeQuery(sql);
                                if(rs1!=null){
                                ProductosVendidos.setCellValueFactory(new PropertyValueFactory<>("ProductosVendidos"));
                                Categoria.setCellValueFactory(new PropertyValueFactory<>("Categoria"));
                            ObservableList<ProductoporCategoria> datos = FXCollections.observableArrayList();
        
                             while (rs1.next()) {
                            String n= rs1.getString("ProductosVendidos");
                            String categoria= rs1.getString("categoria");
                            ProductoporCategoria p1 = new ProductoporCategoria(Integer.parseInt(n),categoria);
                            datos.add(p1);
                            }
        
                            tabproductosvendidos.setItems(datos);
                            tabproductosvendidos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                            stbuscar = "drop view ProductoVendido_Categoria;";
                                PreparedStatement pst12 = conexionbd.prepareStatement(stbuscar);
                                pst12.execute();
                        }else{ stbuscar = "drop view ProductoVendido_Categoria;";
                                st = conexionbd.createStatement();
                                rs = st.executeQuery(stbuscar);
                                    
                                }}
                       if(busqueda.getText().equals("")){
                       stbuscar = "select * from producto;"; 
                       st = conexionbd.createStatement();
                       rs = st.executeQuery(stbuscar);
                       celdas(st,rs);}
                    } catch (SQLException ex) {
                        Logger.getLogger(FXMLVistaTProductoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            }
        });
     }
      public static boolean validarFecha(String fecha) throws SQLException {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
     private void celdas(Statement st,ResultSet rs){
         tabproductosvendidos.setVisible(false);
        tablaProductos.setVisible(true);
        tabprodVxFecha.setVisible(false);
          tabprodNoVxFecha.setVisible(false);
        try {
            ObservableList<Producto> datos = FXCollections.observableArrayList();
            while (rs.next()) {
                if (!rs.getString("Producto_ID").equalsIgnoreCase("0")) {
                    String id_producto1 = rs.getString("Producto_ID");
                    String nombre1 = rs.getString("Nombre");
                    String marca1 = rs.getString("Marca");
                    String stock1 = rs.getString("Stock");
                    String precio1 = rs.getString("Precio_Venta");
                    String categoria1 = rs.getString("Categoria_ID");
                    String proveedor= rs.getString("Proveedor_ID");
                    Producto p1 = new Producto(Integer.parseInt(id_producto1),nombre1,marca1,Integer.parseInt(stock1),Double.parseDouble(precio1),Integer.parseInt(categoria1),proveedor);
                    datos.add(p1);
                }
            }
            
            tablaProductos.setItems(datos);
            tablaProductos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        } catch (SQLException ex) {
            Logger.getLogger(FXMLVistaTProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
    
    @FXML
    private void aggProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProducto.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    
    @FXML
    private void aggCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggCategoria.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();
    }
    @FXML
    private void aggEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggEmpleado.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();}
            
    @FXML
    private void aggProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggProveedor.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }      
            
    @FXML
    private void aggVenta(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggVenta.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show(); 
}
    
    @FXML
    private void aggPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaAggPedid.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
    @FXML
    private void aggClientes(MouseEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("PantallaAggClientes.fxml"));
        Stage stage= new Stage();
            stage.setScene(new Scene(root));
            stage.show();  }   
    
    
   
     @FXML
    private void tabProducto(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaTProducto.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCliente(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCliente.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabEmpleado(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaEmpleados.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabCategoria(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaCategoria.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabProveedor(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaProveedor.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabPedido(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaPedidos.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    
    @FXML
    private void tabVenta(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PantallaTablaVentas.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    @FXML
    private void regreso(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLVistaPrincipal.fxml"));
        Scene scene = new Scene(root);
        ventincio.setScene(scene);
        ventincio.show();
    }
    @FXML
    private void eliminar(MouseEvent event) throws SQLException {
        try{
        Producto p = (Producto) tablaProductos.getSelectionModel().getSelectedItem();
        String eliminar = "DELETE FROM producto where Producto_ID='" + p.getProducto_ID()+ "';";

        Statement st = conexionbd.createStatement();
        st.execute(eliminar);

        String show = "select * from producto";
        Statement st1 = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(show);

        celdas(st,rs);

                } catch (Exception e) {
                    Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
                    mensajeExp.setHeaderText("Advertencia");
                    mensajeExp.setContentText("No has seleccionado ninguna celda");
                    mensajeExp.showAndWait();
                }
    }
}
