/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import Conexion.conexion;
import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.*;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Vargas
*/
public class FXMLAccesoController implements Initializable {
    PreparedStatement ps;
    @FXML
    private Label usuario;
     @FXML
    private TextField txtususario;
     @FXML
    private Label contra;
     @FXML
    private TextField txtcontra;
    private static String va;
    @FXML
    private void handleButtonAction(ActionEvent event) {
        
        try {
            if(verificar()){
            String d=new FXMLVistaTController().datos_empleado(txtususario.getText());  
            va=d;
                System.out.println(d);
            System.out.println("Has ingresado con exito!"+ txtususario.getText());
            Parent root = FXMLLoader.load(getClass().getResource("/abarrotecomercialarias/FXMLVistaPrincipal.fxml"));
            Scene scene = new Scene(root);
            ventincio.setScene(scene);
            ventincio.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();}
            else{System.out.println("error de usuario o contraseña");}
        } catch (IOException ex) {
            Logger.getLogger(FXMLAccesoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
      
    }
    public boolean verificar(){
        int sw=0;
         try {
            
            String verificar = "select * from usuarios where usuario='"+txtususario.getText()+"' and contra='"+txtcontra.getText()+"'";
            Statement st = conexionbd.createStatement();
            ResultSet rs = st.executeQuery(verificar);
            while(rs.next()){
                if(rs.getString(1)==null)
                    sw=0;
                else 
                    sw=1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAccesoController.class.getName()).log(Level.SEVERE, null, ex);
        }if(sw==1) return true;
        else return false;
    }

    public static String getVa() {
        return va;
    }

    
    
    
   
}
