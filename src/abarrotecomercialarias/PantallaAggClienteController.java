/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaAggClienteController implements Initializable {

    @FXML
    private TextField id_cliente;
    @FXML
    private TextField apellido;
    @FXML
    private TextField nombre;
    @FXML
    private TextField telefono;
    @FXML
    private TextField direccion;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btnVolver(MouseEvent event) {
        System.out.println("aqui");
        
        ComercialAriasAcceso.ventPrincipal.setScene(ComercialAriasAcceso.scene);
    }

  
    @FXML
    private void btGuardar_Cliente(MouseEvent event) throws SQLException {
        String linea = "Insert Into cliente (Cliente_ID,nombre,apellido,direccion,Telefono) values ('"+ id_cliente.getText()+ "\',\'"  + nombre.getText() + "\',\'" + apellido.getText() + "\',\'" + direccion.getText()+"\',\'" + telefono.getText() + "\'" + ");";
        PreparedStatement pst = conexionbd.prepareStatement(linea);
        pst.execute();
        JOptionPane.showMessageDialog(null,"Cliente guardado exitosamente.");
        telefono.setText("");
        direccion.setText("");
        apellido.setText("");
        nombre.setText("");
        id_cliente.setText("");
        
        
        
        
    }
    
}
