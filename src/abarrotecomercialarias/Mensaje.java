/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Vargas
 */
public class Mensaje {
    
    public static Alert ventanaEmergente(Stage primaryStage, Alert.AlertType tipo) {

        Alert alerta = new Alert(tipo);

        alerta.setResizable(false);
        alerta.initStyle(StageStyle.UNDECORATED);
        alerta.getDialogPane().getStylesheets().addAll(primaryStage.getScene().getStylesheets());
        return alerta;
    }
    
}
