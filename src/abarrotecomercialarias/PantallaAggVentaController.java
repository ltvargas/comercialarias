/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import static abarrotecomercialarias.ComercialAriasAcceso.ventincio;
import static abarrotecomercialarias.Mensaje.ventanaEmergente;
import entidades.DetalleVenta;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaAggVentaController implements Initializable {

    PreparedStatement ps;
    @FXML
    private ComboBox combo_Prod;
    @FXML
    private TextField txtProducto_id;
    @FXML
    private TextField txtPrecio;
    @FXML
    private TextField txtStock_pro;
    @FXML
    private TextField txtcantidad;
    @FXML
    private TextField txtVenta_id;
    @FXML
    private TextField txt_cliente;
    @FXML
    private TextField txtnombre_cliente;
    @FXML
    private TextField txt_apellido;
    @FXML
    private TextField txtTotal;
    @FXML
    private TextField fechasel;
    @FXML
    private TextField txtIVA;
    @FXML
    private TableView<DetalleVenta> tablefactura;
    @FXML
    private TableColumn<DetalleVenta, String> Precio_Unitario;
    @FXML
    private TableColumn<DetalleVenta, String> CantPro;
    @FXML
    private TableColumn<DetalleVenta, String> Total;
    @FXML
    private TableColumn<DetalleVenta, String> Nombre_Producto;
    private List<DetalleVenta> detalle = new ArrayList<>();
    @FXML
    private TextField txtcanti;
    private String StockMomen;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ocultar();
        Calendar calendar = GregorianCalendar.getInstance();
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        fechasel.setText(sdf.format(date));
        txtVenta_id.setText(Integer.toString(numero_factura()));
        Object[] idarticulo = poblar_combox("producto", "Nombre");
        combo_Prod.getItems().removeAll(idarticulo);
        for (int i = 0; i < idarticulo.length; i++) {
            combo_Prod.getItems().add(idarticulo[i]);
        }
    }

    public int numero_factura() {

        int numero;
        int numero_actual = 0;
        try {
            String ss = "SELECT count(*) as contador FROM venta";
            Statement ste = conexionbd.createStatement();
            ResultSet rs = ste.executeQuery(ss);
            rs.next();
            numero = rs.getInt("contador");
            rs.close();
            numero_actual = numero + 1;
        } catch (SQLException ex) {
            Logger.getLogger(PantallaAggVentaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return numero_actual;
    }

    public Object[] poblar_combox(String tabla, String nombrecol) {
        int registros = 0;
        try {
            String sql = "SELECT count(*) as total FROM " + tabla;
            Statement st = conexionbd.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            registros = rs.getInt("total");
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        Object[] datos = new Object[registros];
        try {
            Statement st = conexionbd.createStatement();
            ResultSet rs = st.executeQuery("SELECT Nombre FROM producto");
            int i = 0;
            while (rs.next()) {
                datos[i] = rs.getObject(nombrecol);
                i++;
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return datos;
    }

    public Object[][] GetTabla(String colName[], String tabla, String sqll) {
        int registros = 0;
        try {
            String sql = "SELECT count(*) as total FROM " + tabla;
            Statement st = conexionbd.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            registros = rs.getInt("total");
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        Object[][] data = new String[registros][colName.length];
        String col[] = new String[colName.length];

        try {
            Statement st = conexionbd.createStatement();
            ResultSet rs = st.executeQuery(sqll);
            int i = 0;
            while (rs.next()) {
                for (int j = 0; j <= colName.length - 1; j++) {
                    col[j] = rs.getString(colName[j]);
                    data[i][j] = col[j];
                }
                i++;
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return data;
    }

    @FXML
    private void BuscarCliente(MouseEvent event) {
        String[] columnas = {"nombre", "apellido"};
        Object[][] resultado = GetTabla(columnas, "cliente", "select nombre,apellido from cliente where Cliente_ID='" + txt_cliente.getText() + "';");
        txtnombre_cliente.setText(resultado[0][0].toString());
        txt_apellido.setText(resultado[0][1].toString());
    }

    @FXML
    private void AccionCombo(ActionEvent event) {
        if (combo_Prod.getValue() != null) {
            String[] columnas = {"Producto_ID", "Precio_Venta", "stock"};
            Object[][] resultado = GetTabla(columnas, "producto", "select Producto_ID, Stock, Precio_Venta from producto where Nombre='" + combo_Prod.getValue().toString() + "';");
            System.out.println(resultado[0][0]);
            txtProducto_id.setText(resultado[0][0].toString());
            txtPrecio.setText(resultado[0][1].toString());
            txtStock_pro.setText(resultado[0][2].toString());
            StockMomen=txtStock_pro.getText();
        }
    }

    public double calcular_IVA() {
        double iva;
        double subtotal = datos_totalfactura();
        iva = (subtotal * 0.12);
        return iva;
    }

    @FXML
    private void btnRegistrarDetalle(MouseEvent event) {

        try {
            String sql = "create  table  IF NOT EXISTS productoxdetalle (\n"
                    + "Nombre varchar(40),\n"
                    + "Unidades INT ,\n"
                    + "Precio_Unidad double,\n"
                    + "Subtotal double\n"
                    + ");";
            PreparedStatement pst1 = conexionbd.prepareStatement(sql);
            pst1.execute();
            int c = Integer.parseInt(txtStock_pro.getText());
            int b = Integer.parseInt(txtcantidad.getText());
            int suma_total = 0;

            if (!txtcantidad.getText().equals("0")) {
                if (c >= b) {
                    txtStock_pro.setText(String.valueOf(c - b));
                    int cant = Integer.parseInt(txtcantidad.getText());
                    double precio = Double.parseDouble(txtPrecio.getText());
                    double valor = cant * precio;
                    String sqlp1 = "select a.Nombre\n"
                            + "from producto as a where a.Producto_ID=" + txtProducto_id.getText() + ";";
                    Statement sttn = conexionbd.createStatement();
                    ResultSet rsnom = sttn.executeQuery(sqlp1);
                    String Nompro = "";
                    if (rsnom.next()) {
                        Nompro = rsnom.getString("Nombre");
                    }

                    String sqlp = "insert into productoxdetalle values('" + Nompro + "'," + txtcantidad.getText() + "," + txtPrecio.getText() + "," + valor + ");";
                    PreparedStatement pst = conexionbd.prepareStatement(sqlp);
                    pst.execute();

                    detalle.add(new DetalleVenta(Integer.parseInt(txtProducto_id.getText()), Nompro, Integer.parseInt(txtcantidad.getText()), Double.parseDouble(txtPrecio.getText()), valor));
                    String sqlll = "select * from productoxdetalle;";

                    Statement stt = conexionbd.createStatement();
                    ResultSet rss = stt.executeQuery(sqlll);
                    Precio_Unitario.setCellValueFactory(new PropertyValueFactory<>("Precio_Unitario"));
                    Nombre_Producto.setCellValueFactory(new PropertyValueFactory<>("Nombre_Producto"));
                    CantPro.setCellValueFactory(new PropertyValueFactory<>("Unidades"));
                    Total.setCellValueFactory(new PropertyValueFactory<>("Subtotal"));

                    ObservableList<DetalleVenta> datos = FXCollections.observableArrayList();

                    while (rss.next()) {

                        String Nombre = rss.getString("Nombre");
                        String uni = rss.getString("Unidades");
                        String preciou = rss.getString("Precio_Unidad");
                        String sub = rss.getString("Subtotal");
                        DetalleVenta dv = new DetalleVenta(Integer.parseInt(txtProducto_id.getText()), Nombre, Integer.parseInt(uni), Double.parseDouble(preciou), Double.parseDouble(sub));
                        datos.add(dv);

                    }

                    tablefactura.setItems(datos);
                    tablefactura.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                } else {
                    JOptionPane.showMessageDialog(null, "El stock del articulo no soporta la venta por favor actualize en stock");
                }
            } else {
                JOptionPane.showMessageDialog(null, "La cantidad no es valida");
            }

            Double valorIVA = calcular_IVA();
            txtIVA.setText(Double.toString(valorIVA));
            Double subbb = datos_totalfactura();
            Double totalfinal = subbb + valorIVA;
            txtTotal.setText(Double.toString(totalfinal));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(PantallaAggVentaController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Double datos_totalfactura() {
        double data = 0;
        try {

            ps = conexionbd.prepareStatement("select sum(subtotal) as total\n"
                    + "from productoxdetalle as a\n"
                    + "join producto as b on b.Nombre=a.Nombre;");
            ResultSet res = ps.executeQuery();
            while (res.next()) {
                data = res.getDouble("total");
            }
            res.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return data;
    }

    @FXML
    private void btnGenerarFactura(MouseEvent event) throws SQLException {
        Date dd = new java.util.Date();
        Date date3 = new java.sql.Date(dd.getTime());
        String linea = "Insert Into venta (Fecha,Descuento,Monto_Final,Cliente_ID) values ('" + date3 + "\',\'" + 0 + "\',\'" + txtTotal.getText() + "\',\'" + txt_cliente.getText() + "\'" + ");";
        PreparedStatement pst = conexionbd.prepareStatement(linea);
        pst.execute();
        for (DetalleVenta d : detalle) {

            String linea2 = "call CrearDetalle_Venta(" + d.getUnidades() + "," + d.getPrecio_Unitario() + "," + d.getSubtotal() + "," + d.getProducto_ID() + "," + txtVenta_id.getText() + ",@result)";

            PreparedStatement pst2 = conexionbd.prepareStatement(linea2);
            pst2.execute();
        }
        String linea3 = "drop table if exists productoxdetalle;";
        PreparedStatement pst3 = conexionbd.prepareStatement(linea3);
        pst3.execute();
        JOptionPane.showMessageDialog(null, "Venta guardada exitosamente.");

    }

    @FXML
    private void actualizar(MouseEvent event) throws SQLException {
        try {
            DetalleVenta dv = tablefactura.getSelectionModel().getSelectedItem();
            String modify = "update productoxdetalle set  Unidades= " + txtcanti.getText()
                    + " where Nombre like '" + dv.getNombre_Producto() + "' ;";

            Statement st = conexionbd.createStatement();
            st.execute(modify);
            String show = "select * from productoxdetalle";
            Statement st1 = conexionbd.createStatement();
            ResultSet rs = st1.executeQuery(show);
            
            int c = Integer.parseInt(StockMomen);
            int b = Integer.parseInt(txtcanti.getText());
            
            txtStock_pro.setText(String.valueOf(c-b));
            
            celdas(st, rs);
            ocultar();
        } catch (Exception e) {
            Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
            mensajeExp.setHeaderText("Advertencia");
            mensajeExp.setContentText("No has seleccionado ninguna celda");
            mensajeExp.showAndWait();
        }
    }

    @FXML
    private void modificar(MouseEvent event) {
        
        try {
            mostrar();
            DetalleVenta dv = tablefactura.getSelectionModel().getSelectedItem();
            txtcanti.setText(String.valueOf(dv.getUnidades()));


            
        } catch (Exception e) {
            ocultar();
            Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
            mensajeExp.setHeaderText("Advertencia");
            mensajeExp.setContentText("No has seleccionado ninguna celda");
            mensajeExp.showAndWait();
        }
    }

    private void mostrar() {
        txtcanti.setVisible(true);
    }

    private void ocultar() {
        txtcanti.setVisible(false);
    }

    @FXML
    private void eliminar(MouseEvent event) throws SQLException {
        try {
            DetalleVenta p = (DetalleVenta) tablefactura.getSelectionModel().getSelectedItem();
            String eliminar = "DELETE FROM productoxdetalle where Nombre='" + p.getNombre_Producto() + "';";
            Statement st = conexionbd.createStatement();
            st.execute(eliminar);

            String show = "select * from productoxdetalle;";
            Statement st1 = conexionbd.createStatement();
            ResultSet rs = st.executeQuery(show);

            celdas(st, rs);
            int c = Integer.parseInt(txtStock_pro.getText());
            int b = p.getUnidades();
            txtStock_pro.setText(String.valueOf(c + b));
        } catch (Exception e) {
            Alert mensajeExp = ventanaEmergente(ventincio, Alert.AlertType.WARNING);
            mensajeExp.setHeaderText("Advertencia");
            mensajeExp.setContentText("No has seleccionado ninguna celda");
            mensajeExp.showAndWait();
        }
    }

    private void celdas(Statement st, ResultSet rs) {

        try {
            ObservableList<DetalleVenta> datos = FXCollections.observableArrayList();

            while (rs.next()) {

                String Nombre = rs.getString("Nombre");
                String uni = rs.getString("Unidades");
                String preciou = rs.getString("Precio_Unidad");
                String sub = rs.getString("Subtotal");
                DetalleVenta dv = new DetalleVenta(Integer.parseInt(txtProducto_id.getText()), Nombre, Integer.parseInt(uni), Double.parseDouble(preciou), Double.parseDouble(sub));
                datos.add(dv);

            }

            tablefactura.setItems(datos);
            tablefactura.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        } catch (SQLException ex) {

            Logger.getLogger(PantallaAggVentaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
