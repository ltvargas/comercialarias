/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abarrotecomercialarias;

import static abarrotecomercialarias.ComercialAriasAcceso.conexionbd;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author User-pc
 */
public class PantallaAggProductoController implements Initializable {
    
     @FXML
    private Label lblnom_pro;
     @FXML
    private TextField txt_nomprod;
     @FXML
    private Label lbl_marca;
     @FXML
    private TextField txt_marca;
     @FXML
    private Label lbl_precio;
     @FXML
    private TextField txt_precio;
     @FXML
    private Label lbl_stock;
      @FXML
    private TextField txt_stock;
   @FXML
    private TextField txtcat;
     @FXML
    private TextField txt_proveedor;
    @FXML
    private ComboBox comboCate;
     @FXML
    private Button btnguardar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Object[] idcategoria = poblar_combox("categoria","Categoria");
        comboCate.getItems().removeAll(idcategoria);
        for(int i=0;i<idcategoria.length;i++)
        {
        comboCate.getItems().add(idcategoria[i]);
        }
    }    

    public Object[] poblar_combox(String tabla, String nombrecol){
      int registros = 0;      
      try{
          String sql = "SELECT count(*) as total FROM " + tabla;
          Statement st = conexionbd.createStatement();
          ResultSet rs = st.executeQuery(sql);
          rs.next();
          registros = rs.getInt("total");
          rs.close();
          }catch(SQLException e){
             System.out.println(e);
          }

    Object[] datos = new Object[registros];
      try{
         Statement st = conexionbd.createStatement();
         ResultSet rs = st.executeQuery("SELECT Categoria FROM categoria");
         int i = 0;
         while(rs.next()){
            datos[i] = rs.getObject(nombrecol);
            i++;
         }
         rs.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return datos;
 }
    
    @FXML
    private void AccionComboCate(ActionEvent event) {
       if(comboCate.getValue()!=null )
     {
            String[] columnas={"Categoria_ID"};
            Object[][] resultado = GetTabla(columnas, "categoria","select Categoria_ID from categoria where Categoria='"+ comboCate.getValue().toString() +"';");
            txtcat.setText(resultado[0][0].toString());
        } 
    }
    
    public Object [][] GetTabla(String colName[], String tabla,String sqll){
        int registros = 0;      
      try{
          String sql = "SELECT count(*) as total FROM " + tabla;
          Statement st = conexionbd.createStatement();
          ResultSet rs = st.executeQuery(sql);
          rs.next();
          registros = rs.getInt("total");
          rs.close();
          }catch(SQLException e){
             System.out.println(e);
          }

    Object[][] data = new String[registros][colName.length];
    String col[] = new String[colName.length];
    
      try{
        Statement st = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(sqll);  
        int i = 0;
        while(rs.next()){
            for(int j=0; j<=colName.length-1;j++){
                col[j] = rs.getString(colName[j]);
                data[i][j] = col[j];
            }
            i++;
         }
         rs.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }
    
    @FXML
    private void btnGuardar(MouseEvent event) throws SQLException {
        String sql = "SET FOREIGN_KEY_CHECKS=0";
        Statement st = conexionbd.createStatement();
        ResultSet rs = st.executeQuery(sql);
        String linea = "Insert Into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID) values ('" + txt_nomprod.getText() + "\',\'" + txt_marca.getText() + "\',\'" + txt_stock.getText()+ "\',\'" + txt_precio.getText() + "\',\'" + txt_proveedor.getText()+ "\',\'" +  txtcat.getText() + "\'" + ");";
        PreparedStatement pst = conexionbd.prepareStatement(linea);
        pst.execute();
        JOptionPane.showMessageDialog(null,"Producto guardado exitosamente.");
        
        String sql1 = "SET FOREIGN_KEY_CHECKS=1";
            Statement st1 = conexionbd.createStatement();
            ResultSet rs1 = st1.executeQuery(sql1);
        txt_nomprod.setText("");
        txt_marca.setText("");
        txt_stock.setText("");
        txt_precio.setText("");
        txt_proveedor.setText("");
   
        
    }




    
}
