use abarrote;

insert into categoria (Categoria,Descripcion) values ("Comestibles","Productos procesados y aptos para el consumo humano.");
insert into categoria (Categoria,Descripcion) values ("Bebidas","Sustancias bebibles");
insert into categoria (Categoria,Descripcion) values ("Lácteos","Productos de la leche o relacionado con ella.");
insert into categoria (Categoria,Descripcion) values ("Empaquetados","Productos sometidos a procesos de empaque");
insert into categoria (Categoria,Descripcion) values ("Especias","Sustancia que se añade a un alimento para darle más sabor");
insert into categoria (Categoria,Descripcion) values ("Confitería","Caramelería y afines");
insert into categoria (Categoria,Descripcion) values ("Detergentes","Productos para el lavado");
insert into categoria (Categoria,Descripcion) values ("Snacks","Alimentos ligeros");
insert into categoria (Categoria,Descripcion) values ("Licores","Productos con alcohol");
insert into categoria (Categoria,Descripcion) values ("Panadería","Productos elaborados con harina.");

insert into cliente values (0952494235,"Arlette","Cotrina","Lizardo García y Domingo Savio","0984954584");
insert into cliente values (0999999999,"Diana","Molina","Alborada 6ta etapa","0983445634");
insert into cliente values (0923423434,"Andrés","Anchundia","Sauces 4","0934567856");
insert into cliente values (0934344554,"John","Rodriguez","Orquideas","0923233434");
insert into cliente values (0959439585,"Karla","Garcés","Sauces 8","0945455667");
insert into cliente values (0923488488,"Diego","Campozano","Samanes 2","0912233445");
insert into cliente values (0922299999,"María","Campoverde","Villa Club","0998877665");
insert into cliente values (0912222222,"Johnny","Mejía","Ciudad Celeste","0998765432");
insert into cliente values (0923332333,"Liliana","Vargas","La Joya","0912234567");
insert into cliente values (0945545555,"Daniel","Reyes","Guasmo Central","0984847564");



insert into proveedor values (0932332333,"Pronaca","www.pronaca.com");
insert into proveedor values (0932423442,"Unilever","www.unilever.com");
insert into proveedor values (0923333333,"Toni","www.tonicorp.com");
insert into proveedor values (0944443333,"La Universal","www.launiversal.com.ec");
insert into proveedor values (0922223333,"Nestlé","ww1.nestle.com.ec");
insert into proveedor values (0955544444,"Coca Cola","www.coca-cola.com");
insert into proveedor values (0965535555,"Sumesa","www.sumesa.com.ec");
insert into proveedor values (0943345455,"Corporación Favorita","www.cfavorita.ec");
insert into proveedor values (0966787888,"Lafabril","www.lafabril.com.ec");
insert into proveedor values (0922388656,"Inalecsa","inalecsa.com");



insert into contactos values ("2581879",0932332333);
insert into contactos values ("2444433",0932423442);
insert into contactos values ("2122121",0923333333);
insert into contactos values ("5465665",0944443333);
insert into contactos values ("3454355",0922223333);
insert into contactos values ("7657666",0955544444);
insert into contactos values ("3455454",0965535555);
insert into contactos values ("3556565",0943345455);
insert into contactos values ("3634444",0966787888);
insert into contactos values ("2435466",0922388656);


insert into producto(Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID) values("Mortadela de Pollo","Mr. Pollo",45,4.7,0932332333,4);
insert into producto(Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID) values("Helados Frigo","Heartbrand",50,5,0932423442,6);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Yogurt","GelaToni",45,3.5,0923333333,2);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Chocolate","Manicho",23,1,0944443333,6);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Leche entera la lechera","La lechera",14,2.5,0922223333,3);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Caldo de gallina criolla","Ranchero",2,1.5,0965535555,5);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Fanta Naranja","Fanta",54,2,0955544444,2);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Aceite vegetal comestible","La Favorita",65,0.5,0943345455,1);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Detergente con Jabón","LavaTodo",34,0.3,0966787888,7);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Bocaditos de maiz","Ryskos",21,0.75,0922388656,8);
insert into producto (Nombre,Marca,Stock,Precio_Venta,Proveedor_ID,Categoria_ID)values("Yogurt Light","GelaToni",4,30,923333333,2);

insert into empleado values (0998988787,"Anahís","García","2000-01-21","0936485936");
insert into empleado values (0933333444,"Elizabeth","Pareja","1998-04-15","0914365749");
insert into empleado values (0944444433,"Johan","Navarro","1999-05-13","0955228899");
insert into empleado values (0966655555,"Juan","Bermeo","2000-06-15","0900886644");
insert into empleado values (0987788888,"Danna","Alejandro","1997-06-18","0900099988");
insert into empleado values (0933333222,"María","Castro","1992-02-10","0944444333");
insert into empleado values (0911111222,"Melanie","Sarmiento","1988-03-14","0984954584");
insert into empleado values (0988774444,"Sebastían","Cornejo","1984-05-25","0976879809");
insert into empleado values (0988888888,"Verónica","Duarte","1998-10-23","0987877676");
insert into empleado values (0955555664,"Valeria","Cerezo","1982-11-01","0900909999");


insert into pedido values (default,"2019-01-12","2019-02-13",0932332333,0998988787);
insert into pedido values (default,"2019-02-23","2019-02-25",0932423442,0933333444);
insert into pedido values (default,"2019-03-28","2019-04-01",0923333333,0944444433);
insert into pedido values (default,"2019-04-14","2019-04-16",0944443333,0966655555);
insert into pedido values (default,"2019-01-14","2019-02-13",0922223333,0987788888);
insert into pedido values (default,"2019-04-02","2019-06-14",0955544444,0933333222);
insert into pedido values (default,"2019-02-16","2019-02-20",0965535555,0911111222);
insert into pedido values (default,"2019-03-07","2019-04-02",0943345455,0988774444);
insert into pedido values (default,"2019-02-09","2019-02-13",0966787888,0988888888);
insert into pedido values (default,"2019-04-10","2019-04-13",0922388656,0955555664);

insert into detalle_pedido values (default,2,4.7,9.4,3,1);
insert into detalle_pedido values (default,3,4,12,10,2);
insert into detalle_pedido values (default,4,0.75,3,5,3);
insert into detalle_pedido values (default,2,2.5,5,4,4);
insert into detalle_pedido values (default,10,1.5,15,8,5);
insert into detalle_pedido values (default,1,0.5,0.5,1,6);
insert into detalle_pedido values (default,2,0.25,0.5,6,7);
insert into detalle_pedido values (default,3,0.75,2.25,7,8);
insert into detalle_pedido values (default,4,4,16,9,9);
insert into detalle_pedido values (default,9,1.5,13.5,10,10);

insert into venta values (default,"2019-02-16",10,123.66,0952494235);
insert into venta values (default,"2019-01-25",5,245.87,0999999999);
insert into venta values (default,"2019-03-24",1,23.67,0923423434);
insert into venta values (default,"2019-04-12",0,145.45,0934344554);
insert into venta values (default,"2018-05-23",15,1233.13,0959439585);
insert into venta values (default,"2018-03-22",50,2333.9,0923488488);
insert into venta values (default,"2019-04-21",20,23.8,0922299999);
insert into venta values (default,"2019-03-13",25,112,0912222222);
insert into venta values (default,"2019-02-19",10,23.6,0923332333);
insert into venta values (default,"2019-01-10",15,56.6,0945545555);

insert into detalle_venta values (default,3,4.7,14.1,2,1);
insert into detalle_venta values (default,5,5,25,4,2);
insert into detalle_venta values (default,6,3,18,1,3);
insert into detalle_venta values (default,3,1,3,3,4);
insert into detalle_venta values (default,2,2.3,4.6,6,5);
insert into detalle_venta values (default,6,1.5,6.9,8,6);
insert into detalle_venta values (default,1,1.75,1.75,5,7);
insert into detalle_venta values (default,8,0.5,4,7,8);
insert into detalle_venta values (default,9,0.25,2.25,10,9);
insert into detalle_venta values (default,2,0.75,3.5,9,10);
insert into detalle_venta values (default,5,3,15,9,1);

INSERT INTO usuarios VALUES ("anagar","1234",998988787);