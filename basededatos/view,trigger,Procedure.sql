/*View,Triger,Porcedure*/

-- ¿Qué productos se vendieron en una fecha determinada?
create view ProductosVendidosXfecha as(
select a.Nombre,a.Precio_Venta,sum(b.Unidades) as cantidadVendida,b.Unidades*a.Precio_Venta as Ganancia
from producto as a
join detalle_venta as b on a.producto_ID=b.Producto_ID
join Venta on b.Venta_ID=Venta.Venta_ID
where Fecha="2019-02-16"
group by(b.Producto_ID));
#drop view ProductosVendidosXfecha;
select * from ProductosVendidosXfecha;

-- 2. ¿Qué productos no se venden durante un rango de fechas determinadas?
create view ProductosNoVendidosXfechas as(
 select a.Nombre,a.Marca,a.Stock,a.Precio_Venta 
from producto as a
where a.Producto_ID NOT IN(select d.Producto_ID from detalle_venta as d
join venta on d.Venta_ID=d.DetalleV_ID
where Fecha> '2019-01-01' and Fecha<'2019-03-31'));
#drop view ProductosNoVendidosXfechas;
select * from ProductosNoVendidosXfechas;
#2019-01-01/2019-03-31

-- 3. Cantidad de productos vendidos por categoría.
create view ProductoVendido_Categoria as(
select count(a.producto_ID) as ProductosVendidos, c.categoria
from producto as a
join categoria as c on a.categoria_ID=c.categoria_ID
join detalle_venta as d on d.producto_ID=a.Producto_ID
join Venta on d.Venta_ID=Venta.Venta_ID
group by c.categoria);
#drop view ProductoVendido_Categoria;
select * from ProductoVendido_Categoria;

-- 4.Los 5 Clientes que realizaron más compras durante un año.
create view MasCompras as(
select count(c.cliente_ID) as Compras,c.nombre,c.apellido
from venta as v
join cliente as c on v.cliente_ID=c.cliente_ID
where year(fecha)=2019
group by c.nombre
order by Compras desc
limit 5);
#drop view MasCompras ;
select * from MasCompras ;

 -- 5.Ver la cantidad disponible de cierto producto.
select *
from producto
where nombre="Fanta Naranja";

-- 6.Saber la venta total de un año determinado.
create view MontoFecha as(
select sum(Monto_Final) as monto
from venta
where year(Fecha)=2018);
#drop view MontoFecha ;
select * from MontoFecha ;
-- otros procesos
-- valor del total en ventas
select sum(subtotal) as total
from venta as a
join detalle_venta as b on b.Venta_ID=a.Venta_ID
where a.Venta_ID=1;

-- Procedure que crea detalles de ventas
 Delimiter %%
 create procedure CrearDetalle_Venta(in Unidades int,in Precio_Unidad double,in Subtotal double,in Producto_ID int, in Venta_ID int,out Realizado boolean)
begin
		if EXISTS (Select Venta_ID From venta Where Venta_ID=Venta_ID) then
		insert into detalle_venta values(default,Unidades,Precio_Unidad,Subtotal,Producto_ID,Venta_ID);
        set Realizado :=true;
        else
        set Realizado :=false;
        end if;
end;
%% Delimiter ;
call 

-- Procedure que crea detalles de pedidos
 Delimiter %%
 create procedure CrearDetalle_Pedido(in Unidades int,in Precio_Unidad double,in Subtotal double,in Producto_ID int, in Pedido_ID int,out Realizado boolean)
begin
		if EXISTS (Select Pedido_ID From pedido Where Pedido_ID=Pedido_ID) then
		insert into detalle_pedido values(default,Unidades,Precio_Unidad,Subtotal,Producto_ID,Pedido_ID);
        set Realizado :=true;
        else
        set Realizado :=false;
        end if;
end;
%% Delimiter ;
call 




--  trigger que disminuye la cantidad de stock en la tabla de productos cuando ha sido comprado
Delimiter %%
create trigger ControlarStock after insert on detalle_venta for each row
begin
	update producto set Stock= Stock - new.Unidades where Producto_ID=new.Producto_ID;
end;
%% Delimiter ;