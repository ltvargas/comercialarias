drop database if exists abarrote;
create database Abarrote;

use Abarrote;
CREATE TABLE IF NOT EXISTS CATEGORIA(
Categoria_ID INT  AUTO_INCREMENT PRIMARY KEY,
Categoria VARCHAR(30),
Descripcion VARCHAR(50));

CREATE TABLE IF NOT EXISTS PROVEEDOR(
Proveedor_ID VARCHAR(10) PRIMARY KEY unique,
Nombre VARCHAR(20),
Web VARCHAR(20));

CREATE TABLE IF NOT EXISTS CONTACTOS(
Num_Telefono VARCHAR(10) PRIMARY KEY,
Proveedor_ID VARCHAR(13) ,
FOREIGN KEY (Proveedor_ID) REFERENCES PROVEEDOR(Proveedor_ID)on delete cascade);

CREATE TABLE IF NOT EXISTS EMPLEADO(
Empleado_ID INT PRIMARY KEY unique,
nombre VARCHAR(30) ,
apellido varchar(30) ,
Fecha_Nacimiento Date,
Telefono VARCHAR(10));

CREATE TABLE IF NOT EXISTS CLIENTE(
Cliente_ID INT  PRIMARY KEY unique,
nombre VARCHAR(30),
apellido varchar(30) ,
direccion varchar(60),
Telefono VARCHAR(10));

CREATE TABLE IF NOT EXISTS VENTA(
Venta_ID INT(20) AUTO_INCREMENT PRIMARY KEY,
Fecha Date,
Descuento Double,
Monto_Final Double,
Cliente_ID INT,
CONSTRAINT FOREIGN KEY (Cliente_ID) REFERENCES CLIENTE(Cliente_ID)on delete cascade);


CREATE TABLE IF NOT EXISTS PEDIDO(
Pedido_ID INT AUTO_INCREMENT PRIMARY KEY,
Fecha_Pedida Date,
Fecha_Entrega Date,
Proveedor_ID VARCHAR(13) ,
Empleado_ID INT,
FOREIGN KEY (Proveedor_ID) REFERENCES PROVEEDOR(Proveedor_ID)on delete cascade,
FOREIGN KEY (Empleado_ID) REFERENCES EMPLEADO(Empleado_ID)on delete cascade);

CREATE TABLE IF NOT EXISTS PRODUCTO(
Producto_ID INT AUTO_INCREMENT PRIMARY KEY,
Nombre VARCHAR(20) ,
Marca VARCHAR(20),
Stock INT ,
Precio_Venta double,
Proveedor_ID VARCHAR(13) ,
Categoria_ID INT ,
FOREIGN KEY (Proveedor_ID) REFERENCES PROVEEDOR(Proveedor_ID) on delete cascade,
FOREIGN KEY (Categoria_ID) REFERENCES CATEGORIA(Categoria_ID) on delete cascade);

CREATE TABLE IF NOT EXISTS DETALLE_PEDIDO(
DetalleP_ID INT AUTO_INCREMENT PRIMARY KEY,
Unidades INT ,
Precio_Unidad double,
Subtotal double,
Producto_ID INT ,
Pedido_ID INT ,
FOREIGN KEY (Producto_ID) REFERENCES PRODUCTO(Producto_ID)on delete cascade,
FOREIGN KEY (Pedido_ID) REFERENCES PEDIDO(Pedido_ID)on delete cascade);

CREATE TABLE IF NOT EXISTS DETALLE_VENTA(
DetalleV_ID INT  AUTO_INCREMENT PRIMARY KEY,
Unidades INT ,
Precio_Unidad double ,
Subtotal double,
Producto_ID INT ,
Venta_ID INT ,
FOREIGN KEY (Producto_ID) REFERENCES PRODUCTO(Producto_ID)on delete cascade,
FOREIGN KEY (Venta_ID) REFERENCES VENTA(Venta_ID)on delete cascade);

Create table IF NOT EXISTS usuarios(
usuario VARCHAR(10) not null primary key unique, 
contra VARCHAR(20) not null
);
INSERT INTO usuarios VALUES ("ltvargas","1234");
alter table usuarios
add Empleado_ID int(11) ;
alter table usuarios
add FOREIGN KEY (Empleado_ID) REFERENCES empleado(Empleado_ID)on delete cascade;
Delete From usuarios Where usuario="ltvargas";

alter table categoria modify Descripcion varchar(100);
alter table proveedor modify web varchar(30);
ALTER TABLE pedido ADD CONSTRAINT CHECK(Fecha_Pedida<Fecha_Entrega);
alter table producto modify Nombre varchar(50);